import React from 'react';
import {Render014ContextParent} from './components/Render011ContextAndSameElementReference/Render014ContextParent';
import {Render014ContextChildA} from './components/Render011ContextAndSameElementReference/Render014ContextChild';

const App = () => {
  return (
    <>
      {/* <Render001UseState/> */}
      {/* <Render002UseReducer/> */}
      {/* <Render0032ArrayUseState /> */}
      {/* <Render004Parent/> */}
      {/* <Render005Parent>*/}
      {/* <Render005Child />*/}
      {/* </Render005Parent> */}
      {/*<Render005GrandParent/>*/}
      {/*<Render006Parent/>*/}
      {/*<Render007Parent/>*/}
      {/* <Render008Parent/> */}
      {/* <Render009Parent/> */}
      {/* <Render010Parent /> */}
      {/* <Render011Parent /> */}
      {/* <Render012ContextParent /> */}
      {/* <Render013ContextParent /> */}
      <Render014ContextParent>
          <Render014ContextChildA/>
      </Render014ContextParent>
    </>
  );
};

export default App;
