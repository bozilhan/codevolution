import React from 'react';
import {CountContext} from './Render014ContextParent';

const Render014ContextChildA = () => {
    console.log('Render014ContextChildA');

    return (
      <>
        <div>Child A</div>
        <Render014ContextChildB />
      </>
    );
}

 const Render014ContextChildB = () => {
    console.log('Render014ContextChildB');
    
    return (
      <>
        <div>Child B</div>
        <Render014ContextChildC />
      </>
    );
}

 const Render014ContextChildC = () => {
     const count = React.useContext(CountContext);
    
     console.log('Render014ContextChildC');
    
    return (
      <>
        <div>Child C count= {count}</div>
      </>
    );
}

export {
  Render014ContextChildA,
  Render014ContextChildB,
  Render014ContextChildC
};