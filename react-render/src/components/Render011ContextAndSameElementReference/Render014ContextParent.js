import React from 'react';

export const CountContext = React.createContext();
const CountProvider = CountContext.Provider;

/*
destucturing childen from component props 
and specify the same as children to the context provider

in App.js we have to slightly modify the context parent invocation 
*/  
export const Render014ContextParent = ({ children }) => {
  const [count, setCount] = React.useState(0);

  console.log('Render014ContextParent');

  return (
    <>
      <button onClick={() => setCount(c => c + 1)}>Count - {count}</button>
      <CountProvider value={count}>{children}</CountProvider>
    </>
  );
};
