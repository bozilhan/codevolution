import React from 'react';
import {Render005Parent} from './Render005Parent';
import {Render005Child} from './Render005Child';

export const Render005GrandParent = () => {
    const [newCount, setNewCount] = React.useState(0);

    return (
        <>
            <button onClick={() => setNewCount(nc => nc + 1)}>
                Grandparent count - {newCount}
            </button>
            <Render005Parent newCount={newCount}>
                <Render005Child/>
            </Render005Parent>
        </>
    );
};

