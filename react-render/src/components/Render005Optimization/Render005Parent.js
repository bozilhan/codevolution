import React from 'react';

export const Render005Parent = ({children}) => {
    const [count, setCount] = React.useState(0);

    console.log('Render005Parent');

    return (
      <div>
        <button onClick={() => setCount(c => c + 1)}>Count - {count}</button>
        {children}
      </div>
    );
}
