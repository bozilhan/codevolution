/* 
RENDERING BEHAVIOUR 
WHEN WE ARE DEALING WITH useState hook

after the initial render one of the ways to flag a component for re-render
is by CALLING the setter function from useState

with every subsequent click the setCount function will FLAG or QUEUE 
a re-render of our component and the message is logged in the console every time

if I reload clear the console and click on the `Count to 0` button the component does NOT RE-RENDER
so after the initial render if you call a setter function but set the state to the same value component will NOT RE-RENDER
*/
import React from 'react';

export const Render001UseState = () => {
    const [count, setCount] = React.useState(0);

    console.log('Render001UseState');

    return (
      <div>
        <button onClick={() => setCount(c => c + 1)}>Count - {count}</button>
        <button onClick={() => setCount(0)}>Count to 0</button>
        <button onClick={() => setCount(5)}>Count to 5</button>
      </div>
    );
}
