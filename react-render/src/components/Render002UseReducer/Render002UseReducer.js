import React from 'react';

const initialState = 0;

const reducer =  (state,action)=>{
  switch (action) {
    case 'increment':
      return state + 1;
    case 'decrement':
      return state - 1;
    case 'reset':
      return initialState;
    default:
      return state;
  }
}

export const Render002UseReducer = () => {
    const [count, dispatch] = React.useReducer(reducer, initialState); 
    
    console.log('Render002UseReducer');

    return (
      <div>
        <div>{count}</div>
        <button onClick={() => dispatch('increment')}>Increment</button>
        <button onClick={() => dispatch('decrement')}>Decrement</button>
        <button onClick={() => dispatch('reset')}>Reset</button>
      </div>
    );
}
