import React from 'react';
import {Render012ContextChildA} from './Render012ContextChild';

export const CountContext = React.createContext();
const CountProvider = CountContext.Provider;

export const Render012ContextParent = () => {
    const [count, setCount] = React.useState(0);

    console.log('Render012ContextParent');
    
    return (
      <>
        <button onClick={() => setCount(c => c + 1)}>Count - {count}</button>
        <CountProvider value={count}>
          <Render012ContextChildA />
        </CountProvider>
      </>
    );
}
