import React from 'react';
import {CountContext} from './Render012ContextParent';

const Render012ContextChildA = () => {
    console.log('Render012ContextChildA');

    return (
      <>
        <div>Child A</div>
        <Render012ContextChildB />
      </>
    );
}

 const Render012ContextChildB = () => {
    console.log('Render012ContextChildB');
    
    return (
      <>
        <div>Child B</div>
        <Render012ContextChildC />
      </>
    );
}

 const Render012ContextChildC = () => {
     const count = React.useContext(CountContext);
    
     console.log('Render012ContextChildC');
    
    return (
      <>
        <div>Child C count= {count}</div>
      </>
    );
}


export {
  Render012ContextChildA,
  Render012ContextChildB,
  Render012ContextChildC
};