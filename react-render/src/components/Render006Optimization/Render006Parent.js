import React from 'react';
import {MemoizedRender006Child} from './Render006Child';

export const Render006Parent = () => {
    const [count, setCount] = React.useState(0);
    const [name, setName] = React.useState('Vishwas');

    console.log('Render006Parent');

    return (
        <div>
            <button onClick={() => setCount(c => c + 1)}>Count - {count}</button>
            <button onClick={() => setName('codevolution')}>Change Name</button>
            <MemoizedRender006Child name={name}/>
        </div>
    );
};
