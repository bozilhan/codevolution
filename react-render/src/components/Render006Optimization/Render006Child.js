import React from 'react';

export const Render006Child = () => {
    console.log('Render006Child');
    return (
        <div>
            Render006Child Component
        </div>
    )
}

/*
React.memo is a higher order component which you can use to wrap components
if they are render the sane result given the same props

if your component props dont change between renders react will skip rendering the component
and reuse the last rendered result

in our case react will skip rendering the child component and we use the render output from the previous render

memo only does a shallow comparison of the previous and new props
however you can pass in a custom comparison function as the second argument to React.memo to meet your requirements
* */
export const MemoizedRender006Child = React.memo(Render006Child);