import React from 'react';
import {MemoizedRender013ContextChildA} from './Render013ContextChild';

export const CountContext = React.createContext();
const CountProvider = CountContext.Provider;

export const Render013ContextParent = () => {
    const [count, setCount] = React.useState(0);

    console.log('Render013ContextParent');
    
    return (
      <>
        <button onClick={() => setCount(c => c + 1)}>Count - {count}</button>
        <CountProvider value={count}>
          <MemoizedRender013ContextChildA />
        </CountProvider>
      </>
    );
}
