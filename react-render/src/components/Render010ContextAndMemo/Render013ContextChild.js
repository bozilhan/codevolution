import React from 'react';
import {CountContext} from './Render013ContextParent';

const Render013ContextChildA = () => {
    console.log('Render013ContextChildA');

    return (
      <>
        <div>Child A</div>
        <Render013ContextChildB />
      </>
    );
}

/**
 instead of exporting Render013ContextChildA
 we export MemoizedRender013ContextChildA
 */

const MemoizedRender013ContextChildA = React.memo(Render013ContextChildA);

 const Render013ContextChildB = () => {
    console.log('Render013ContextChildB');
    
    return (
      <>
        <div>Child B</div>
        <Render013ContextChildC />
      </>
    );
}

 const Render013ContextChildC = () => {
     const count = React.useContext(CountContext);
    
     console.log('Render013ContextChildC');
    
    return (
      <>
        <div>Child C count= {count}</div>
      </>
    );
}

export {
  Render013ContextChildA,
  MemoizedRender013ContextChildA,
  Render013ContextChildB,
  Render013ContextChildC
};