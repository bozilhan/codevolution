import React from 'react';
import {Render004Child} from './Render004Child';

export const Render004Parent = () => {
    const [count, setCount] = React.useState(0);

    console.log('Render004Parent');

    return (
      <div>
        <button onClick={() => setCount(c => c + 1)}>Count - {count}</button>
        <button onClick={() => setCount(0)}>Count to 0</button>
        <button onClick={() => setCount(5)}>Count to 5</button>
        <Render004Child />
      </div>
    );
}
