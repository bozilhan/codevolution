import React from 'react';

const INITIAL_STATE = {
  fname: 'Bruce',
  lname: 'Wayne'
};

export const Render0031ObjectUseState = () => {
  const [person, setPerson] = React.useState(INITIAL_STATE);

  const changeName = () => {
    /*
        !!! DIRECTLY MUTATING the OBJECT and EXPECTING the component to RE-RENDER. !!!

        person.fname  = 'Clark'
        person.lname = 'Kent';
        setPerson(person);

        properties of the person object and then calling the setter function however you can see that the component does NOT RE-RENDER
        this is again because of the OBJECT.EASE ALGORITHM that react uses
        when we use object as state the reference to the object must change for the component to queue a re-render 
        after comparing the old and the new state
        this is a COMMON MISTAKE we tend to make as beginners. 


        to FIX this CREATE THE COPY OF OLD STATE by using SPREAD OPERATOR
      */
    const newPerson = { ...person };
    newPerson.fname = 'Clark';
    newPerson.lname = 'Kent';

    setPerson(newPerson);
  };

  console.log('Render0031ObjectUseState');
  
  return (
    <div>
      <button onClick={changeName}>
        {person.fname} {person.lname}
      </button>
    </div>
  );
}
