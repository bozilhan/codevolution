import React from 'react';

const INITIAL_STATE = ['Bruce', 'Wayne'];

export const Render0032ArrayUseState = () => {
    const [persons, setPersons] = React.useState(INITIAL_STATE);
    
    const handleClick = () => {
        /*
        if I clear the console click on the button we DONT SEE the component RE-RENDERING 
        this again due to fact that ARRAY REFERENCE did NOT CHANGE for react to queue up a re-render 
        when you push elements into the same array the ARRAY VALUES CHANGE 
        but the array REFERENCE ITSELF DOES NOT CHANGE so react does NOT RE-RENDER the component 
        
        persons.push('Clark');
        persons.push('Kent');
        setPersons(persons);
        
        
        to FIX this, MAKE a COPY of ARRAY by using SPREAD OPERATOR 
        and PUSH ITEMS and THEN PASS the NEW ARRAY into the SETTER FUNCTION
        */
       const newPersons = [...persons];
       newPersons.push('Clark')
       newPersons.push('Kent')

       setPersons(newPersons);
    };
    
    console.log('Render0032ArrayUseState');

    return (
      <div>
        <button onClick={handleClick}>Click</button>
        {persons.map(person => (
          <div key={person}>{person}</div>
        ))}
      </div>
    );
}
