import React from 'react';
import {MemoizedRender010Child} from './Render010Child';

/*
https://www.youtube.com/watch?v=v3J9hYAMVPc&list=PLC3y8-rFHvwg7czgqpQIBEAHn8D6l530t&index=13
React Render Tutorial - 13 - useMemo
* */
export const Render010Parent = () => {
  const [count, setCount] = React.useState(0);
  const [name, setName] = React.useState('Vishwas');

  const person = {
    fname: 'Bruce',
    lname: 'Wayne'
  };

  /*
  memoized version of person object using useMemo hook
  */
  const memoizedPerson = React.useMemo(() => person
    /*
    this function creates the person object 
    which will be memoized
    */ 
  ,[]);

  console.log('Render010Parent');

  return (
    <div>
      <button onClick={() => setCount(c => c + 1)}>Count - {count}</button>
      <button onClick={() => setName('codevolution')}>Change Name</button>
      <MemoizedRender010Child name={name} person={memoizedPerson} />
    </div>
  );
};
