import React from 'react';
import {MemoizedRender011Child} from './Render011Child';

/*
https://www.youtube.com/watch?v=v3J9hYAMVPc&list=PLC3y8-rFHvwg7czgqpQIBEAHn8D6l530t&index=13
React Render Tutorial - 13 - useCallback
* */
export const Render011Parent = () => {
  const [count, setCount] = React.useState(0);
  const [name, setName] = React.useState('Vishwas');

  const handleClick = () => {};
  
  const memoizedHandleClick = React.useCallback(handleClick, []);

  console.log('Render011Parent');

  return (
    <div>
      <button onClick={() => setCount(c => c + 1)}>Count - {count}</button>
      <button onClick={() => setName('codevolution')}>Change Name</button>
      <MemoizedRender011Child name={name} handleClick={memoizedHandleClick} />
    </div>
  );
};
