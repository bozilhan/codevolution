import React from 'react';

export const Render011Child = ({ name }) => {
  const date = new Date();

  console.log('Render011Child');

  return (
    <>
      Hello {name}
    </>
  );
};

export const MemoizedRender011Child = React.memo(Render011Child);
