import React from 'react';

export const Render010Child = ({ name, person }) => {
  const date = new Date();

  console.log('Render010Child');

  return (
    <>
      Hello {name} {person.fname} {person.lname}
    </>
  );
};

export const MemoizedRender010Child = React.memo(Render010Child);
