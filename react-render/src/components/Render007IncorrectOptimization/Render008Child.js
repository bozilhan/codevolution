import React from 'react';

export const Render008Child = ({name}) => {
    const date = new Date();

    console.log('Render008Child');
    return (
        <>
            Hello {name}. It is currently {date.getHours()}:{date.getMinutes()}
            :{date.getSeconds()}
        </>
    );
};

export const MemoizedRender008Child = React.memo(Render008Child);