import React from 'react';

export const Render009Child = ({ name}) => {
  const date = new Date();

  console.log('Render009Child');
  return <>Hello {name}</>;
};

export const MemoizedRender009Child = React.memo(Render009Child);