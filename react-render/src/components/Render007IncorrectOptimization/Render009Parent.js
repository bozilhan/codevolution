import React from 'react';
import {MemoizedRender009Child} from './Render009Child';

/*
https://www.youtube.com/watch?v=df6OFeIl2l0&list=PLC3y8-rFHvwg7czgqpQIBEAHn8D6l530t&index=12
React Render Tutorial - 12 - Incorrect memo with props Reference
* */

export const Render009Parent = () => {
  const [count, setCount] = React.useState(0);
  const [name, setName] = React.useState('Vishwas');

  const person = {
    fname: 'Bruce',
    lname: 'Wayne'
  };

  /*
  we are not concerned about the implementation
  so leave the function body empty
  */
  const handleClick = () => {};

  console.log('Render009Parent');

  return (
    <div>
      <button onClick={() => setCount(c => c + 1)}>Count - {count}</button>
      <button onClick={() => setName('codevolution')}>Change Name</button>
      <MemoizedRender009Child name={name} handleClick={handleClick} />
    </div>
  );
};
