import React from 'react';
import {MemoizedRender007Child} from './Render007Child';

/*
* https://www.youtube.com/watch?v=fVnnPImZ6a8&list=PLC3y8-rFHvwg7czgqpQIBEAHn8D6l530t&index=10
* React Render Tutorial - 10 - Incorrect memo with children
* */

export const Render007Parent = () => {
    const [count, setCount] = React.useState(0);
    const [name, setName] = React.useState('Vishwas');

    console.log('Render007Parent');

    return (
        <div>
            <button onClick={() => setCount(c => c + 1)}>Count - {count}</button>
            <button onClick={() => setName('codevolution')}>Change Name</button>
            <MemoizedRender007Child name={name}>
                <strong>Hello</strong>
            </MemoizedRender007Child>
        </div>
    );
};
