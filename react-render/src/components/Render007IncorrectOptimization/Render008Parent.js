import React from 'react';
import {MemoizedRender008Child} from './Render008Child';

/*
https://www.youtube.com/watch?v=orrme2ydRfs&list=PLC3y8-rFHvwg7czgqpQIBEAHn8D6l530t&index=11
* React Render Tutorial - 11 - Incorrect memo with Impure Component
* */

export const Render008Parent = () => {
    const [count, setCount] = React.useState(0);
    const [name, setName] = React.useState('Vishwas');

    console.log('Render008Parent');

    return (
        <div>
            <button onClick={() => setCount(c => c + 1)}>Count - {count}</button>
            <button onClick={() => setName('codevolution')}>Change Name</button>
            <MemoizedRender008Child name={name}/>
        </div>
    );
};
