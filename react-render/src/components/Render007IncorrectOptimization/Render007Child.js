import React from 'react';

export const Render007Child = ({children, name}) => {
    console.log('Render007Child');
    return (
        <div>
            Render007Child Component {children} {name}
        </div>
    );
};

export const MemoizedRender007Child = React.memo(Render007Child);