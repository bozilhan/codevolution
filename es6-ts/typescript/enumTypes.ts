/*
    enum type is a way of giving more friendly names to a 
    set of NUMERIC VALUES

    enum values BEGIN WITH 0 

    if you want the number starting at a DIFFERENT value 
    specify that in the enum declaration

*/

enum Color {
  Red, // 0
  Green, //1
  Blue //2
}

enum Color2 {
  Red = 5,
  Green, //6
  Blue // 7
}

const c: Color = Color.Green;
console.log(c); // 1

const c2: Color2 = Color2.Green;
console.log(c2); // 6
