/* 
    CLASSES
    2 classes are EQUIVALENT
    by default each class member is PUBLIC
*/
class Person {
  public fname: string;
  public lname: string;
  constructor(fname: string, lname: string) {
    this.fname = fname;
    this.lname = lname;
  }
}

class Person1 {
  constructor(public fname: string, public lname: string) {}
}

/*
  INTERFACES
  interfaces are the most flexible way of describing TYPES in typescript
  because the TYPE INFORMATION IS ERASED from a typescript program when we COMPILE it
  we DONT HAVE TO WORRY ABOUT RUNTIME OVERHEAD we add when USING INTERFACES

  to make property OPTIONAL we just have to add ? NEXT TO PARTICULAR PROPERTY

  only purpose of using interface in typescript is to DESCRIBE A PARTICULAR TYPE
*/

interface PersonInterface {
  // fname and lname are MANDATORY properties for PersonInterface
  fname: string;
  lname: string;
  // age property is OPTIONAL DUE TO ?
  age?: number;
}

const employee1: PersonInterface = {
  fname: 'chandler',
  lname: 'bing',
  age: 37
};

const employee2: PersonInterface = {
  fname: 'ross',
  lname: 'gellar'
};
