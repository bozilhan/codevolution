/*
    https://www.youtube.com/watch?v=WBPrJSw7yQA
    21:45

    if you are UNSURE what a variable type would be
    make use of the ANY TYPE

    if you expect a value from 3rd library or user input that the value is DYNAMIC
    make use of the ANY TYPE and that will ALLOW you to REASSIGN DIFFERENT types of values
*/

/*
    compiler DOESNT THROW an ERROR!!! because the types of variables are ANY
*/
let randomValue: any = 20;
randomValue = true;
randomValue = 'vishwas';

let myVariable: any = 10;
console.log(myVariable.name);
myVariable();
myVariable.toUpperCase();

/*
    unknown type is very similar to the any type
    however we CANNOT ACCESS any properties of an unknown type
    NOR you can CALL or CONSTRUCT them
*/

/*
    compiler THROW an ERROR!!!
    console.log(myVariable.name);
    myVariable();

    asagidaki adimlar kodun hata almasini engelleyen adimlar
*/
let myUnknownTypeVariable: unknown = 10;

/*
    USER DEFINED TYPE GUARD

    the function that checks if an object has a name property or not

    the function has parameter object of type any
    it returns an object which contains the name property as string
*/
function hasName(obj: any): obj is {name: string} {
  return !!obj && typeof obj === 'object' && 'name' in obj;
}

if (hasName(myUnknownTypeVariable)) {
  console.log(myUnknownTypeVariable.name);
}

/*
    type assertion is similar to type casting in other languages
    we say that myVariable should be treated as a string
    and then toUpperCase() can be applied
*/
(myUnknownTypeVariable as string).toUpperCase();

/*
    multi type
*/
let multiType: number | boolean;
multiType = 20;
multiType = true;
