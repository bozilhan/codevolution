/*
    https://www.youtube.com/watch?v=Psdf5Bo1SFM&list=PLC3y8-rFHvwhI0V5mE9Vu6Nm-nap8EcjV&index=36
    https://www.youtube.com/watch?v=Lo70ypUOHNk&list=PLC3y8-rFHvwhI0V5mE9Vu6Nm-nap8EcjV&index=37
    SYMBOLS
    NEW PRIMITIVE TYPE introduced ES6

    PURPOSE of a symbol is to GENERATE a UNIQUE ID
    but we NEVER GET ACCESS to that ID

    SYMBOL REGISTRY
    use created symbol in a different or the same file
    for that purpose we have builtin symbol REGISTRY
    to add to symbol to the registry we use symbol.form() method
*/

/*
    create a new symbol 
    if you try to use new keyword it is going to throw an ERROR
*/
const s = Symbol();
console.log(typeof s); // symbol

/*
    we can also pass an OPTIONAL symbol DESCRIPTION while creating a symbol
    we can get access to description through .toString()
*/
const describedSymbol = Symbol('Optional Description');
console.log(describedSymbol.toString()); // Symbol(Optional Description)

/*
    symbol always creates a UNIQUE ID
    s2 has different ID and s3 has different ID
    IT DOESNT MATTER what the description is!!!
*/
const s2 = Symbol('Test');
const s3 = Symbol('Test');
console.log(s2 === s3); // false

/*
    when we did registeredSymbol on the first time 
    a new symbol was created in the global registry with this description
    and Symbol.for is called again WITH THE SAME DESCRIPTION this symbol
    was RETRIEVED and STORED in file
    now registeredSymbol and registeredSymbol2 are both the SAME
*/
const registeredSymbol = Symbol.for('RegisteredSymbol');
const registeredSymbol2 = Symbol.for('RegisteredSymbol');
console.log(registeredSymbol === registeredSymbol2); // true

console.log(Symbol.keyFor(registeredSymbol)); // RegisteredSymbol

/*
    we have created a UNIQUE property inside this object
    we never have to worry about our code conflicting with existing methods
    or being accidentally overwritten becuse this property is always UNIQUE
*/
const fName = Symbol();

let person = {
  [fName]: 'Chandler'
};

console.log(Object.getOwnPropertyNames(person)); // []
console.log(Object.getOwnPropertySymbols(person)); // Symbol

/*
    Symbol iterator
    which of these types can be used with the for..of loop
    function --> that means we can use this type in a for..of loop (string, array)
    undefined --> that means we CANNOT USE the for..of loop with specific type (number,object)

    that is the main usage of Symbol.iterator to CHECK for..of loop can be used for particular type

    OBJECTS ARE NOT ITERABLE we can WRITE OWN SPECIAL METHOD
*/
const str = 'hello';
const arr = [1, 2, 3];
const num = 5;
const obj = {name: 'chandler'};

console.log('For string - ' + typeof str[Symbol.iterator]); // function
console.log('For array - ' + typeof arr[Symbol.iterator]); // function
console.log('For number - ' + typeof num[Symbol.iterator]); // undefined
console.log('For object - ' + typeof obj[Symbol.iterator]); // undefined
