/*
    https://www.youtube.com/watch?v=z79gEQLlc_s&list=PLC3y8-rFHvwhI0V5mE9Vu6Nm-nap8EcjV&index=30    COLLECTIONS
    https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Set
    SET
    set is nothing but a list of values but this list CANNOT CONTAIN ANY DUPLICATES
    set is a data structure which contains list of values that are UNIQUE
    when you try to insert or add a DUPLICATE value it is going to IGNORE that particular value
    unlike arrays we CANNOT access INDIVIDUAL ELEMENTS in set
    in sets we just CHECK IF A VALUE IS PRESENT OR NOT
    we CANT ACCESS the VALUE
    set is called as a STRONG SET because it STORES OBJECT REFERENCES!!!
*/

const mySet = new Set();
const ob1 = {};
const ob2 = {};

/*
    add elements to set by calling add() method

    to check the length of set we use SIZE property
*/
mySet.add('Hello');
mySet.add(2);
mySet.add(ob1);
mySet.add(ob2);

/*
    objects are NOT CONVERTED to strings the two objects will be UNIQUE
*/
console.log(mySet.size); // 4

const newSet = new Set([1, 2, 3, 4, 4, 4]);
console.log(newSet.size); // 4

/*
    has method
*/
console.log(newSet.has(1)); // true
console.log(newSet.has(5)); // false

/*
    delete
    Removes the element associated to the value and returns the value that 
    Set.prototype.has(value) would have previously returned. 
    Set.prototype.has(value) will return false afterwards.
*/
console.log(newSet.delete(1)); // true

const chainSet = new Set().add('Hello').add('world');

const strongSet = new Set();
let obj1 = {key: 37};
strongSet.add(obj1);
console.log(strongSet.size); // 1
obj1 = null;
/*
    even though the obj1 was set to null a reference to the obj1 object
    STILL EXISTED in strongSet
*/
console.log(strongSet.size); // 1
/*
    obj1 = [...strongSet][0]; gives us the OBJECT BACK!!!
*/
obj1 = [...strongSet][0];
console.log(obj1); // {key:37}

/*
    https://www.youtube.com/watch?v=-t-aMzm3ngo&list=PLC3y8-rFHvwhI0V5mE9Vu6Nm-nap8EcjV&index=31
    WEAK SET
    aid with GARBAGE COLLECTION and AVOID MEMORY LEAKS 
    we would prefer that the REFERENCE in set to DISAPPEAR when all other references DISAPPEAR
    for this purpose WEAK SET is used

    WEAK SET can STORE ONLY OBJECT REFERENCES and NOT PRIMITIVE!!!
    and object references are WEAK

    the only ADVANTAGE of WeakSet over StrongSet is MEMORY is HANDLED PROPERLY with WeakSet
*/
const weakSet = new WeakSet();
let obj1 = {key: 37};
weakSet.add(obj1);
console.log(weakSet.has(obj1)); // true
/*
    the reference to obj1 in weakSet is NO LONGER ACCESSIBLE
*/
obj1 = null;

/*
    https://developer.mozilla.org/tr/docs/Web/JavaScript/Reference/Global_Objects/Map
    https://www.youtube.com/watch?v=4fjmYzEXdWo&list=PLC3y8-rFHvwhI0V5mE9Vu6Nm-nap8EcjV&index=32
    MAP 
    map is collection of KEY,VALUE PAIRS
    map is an ORDERED LIST(key value pair eklendigi sirada tutulur) of KEY,VALUE pairs
    in map both key and value can be ANY TYPE
    unlike the OBJECTS where the type of the PROPERTY(KEY) is ALWAYS a STRING
    we can also USE OBJECTS AS KEYS within a map
    we can use SIZE PROPERTY to know how many key,value pairs are present in a particular map
    we can use HAS METHOD TO CHECK if a key EXISTS in a map
    we have a DELETE METHOD TO REMOVE a key,value pair
    we have a CLEAR METHOD that REMOVES ALL the key,value pairs from a map
    we can use array for map initialization
*/
const map = new Map();

/*
    to add item to map we use SET METHOD. we specify KEY,VALUE PAIR
*/
map.set('fname', 'chandler');
map.set('age', 30);

/*
    RETRIEVE a value that is set we use GET METHOD
    we HAVE TO PASS the KEY
*/
console.log(map.get('fname')); // chandler

/*
    we can also USE OBJECTS AS KEYS within a map
*/
const obj1 = {};
const obj2 = {};
map.set(obj1, 10);
map.set(obj2, 20);
console.log(map.get(obj1)); // 10

/*
    we can use SIZE PROPERTY to know how many key,value pairs are present in a particular map
*/
console.log(map.size); // 4

/*
    we can use HAS METHOD TO CHECK if a key EXISTS in a map
*/
console.log(map.has('fname')); // true

/*
    we can also have a DELETE METHOD TO REMOVE a key,value pair
    we HAVE TO SPECIFY the KEY
*/
map.delete('fname');

/*
  we have a CLEAR METHOD that REMOVES ALL the key,value pairs from a map  
*/
map.clear();
console.log(map.size); // 0

/*
    we can use array for map initialization
*/
const mapFromArray = new Map(['fname', 'chandler'], ['lname', 'bing']);

for (const key of mapFromArray.keys()) {
  console.log(key); // fname \n lname
}

for (const value of mapFromArray.values()) {
  console.log(value); // chandler \n bing
}

for (const entry of mapFromArray.entries()) {
  console.log(`${entry[0]} -> ${entry[1]}`); // fname -> chandler \n lname -> bing
}

/*
    destructuring
*/
for (const [key, value] of mapFromArray.entries()) {
  console.log(`${key} -> ${value}`); // fname -> chandler \n lname -> bing
}

/*
    https://www.youtube.com/watch?v=SoQS2mJIDfU&list=PLC3y8-rFHvwhI0V5mE9Vu6Nm-nap8EcjV&index=35
    WEAK MAP

    KEYS can ONLY be OBJECT and NOT PRIMITIVE!!!
*/
const weakMap = new WeakMap();
const obj1 = {
  37: 'barz'
};
weakMap.set(obj1, 'never give up');
console.log(weakMap.get(obj1)); // never give up

/*
    at this point weakMay is empty
*/
obj1 = null;
