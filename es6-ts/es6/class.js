/*
    https://www.youtube.com/watch?v=LfIoymnF6Mg&list=PLC3y8-rFHvwhI0V5mE9Vu6Nm-nap8EcjV&index=21
    classes are in fact(aslinda) JUST SPECIAL FUNCTIONS but 
    only DIFFERENCE is that classes unlike(aksine,farkli olarak) functions ARE NOT HOISTED 

    ayni dosya icerisinde sinif tanimindan once ilgili siniftan instance yaratamayiz
*/

class Person {
  constructor(name) {
    this.name = name;
    console.log(this.name + 'Person Constructor');
  }

  static staticMethod() {
    console.log('Static Method');
  }

  greetPerson() {
    console.log('hello' + this.name);
  }

  getId() {
    return 10;
  }
}

const p = new Person('Chandler');
Person.staticMethod();

class Employee extends Person {
  constructor(name) {
    /*
         the parent class constructor needs to be called before the subclass constructor 
         call the constructor from the parent class 
    */
    super(name);
    console.log(this.name + 'Employee Constructor');
  }

  getId() {
    return super.getId();
  }
}

const e = new Employee('Chandler');
