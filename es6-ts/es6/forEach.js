/*
    https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/forEach
    https://www.youtube.com/watch?v=sWEGv9iS2BA&list=PLC3y8-rFHvwhI0V5mE9Vu6Nm-nap8EcjV&index=34
    arrow functionun 1. PARAMETRESI currentElement
    arrow functionun 2. PARAMETRESI INDEX
    foreach(
        (currentValue,index)=>{
            DO SOMETHING
        }
    )

    MAP FOREACH
    parametrelerin SIRALAMASI ONEMLI!!!
    arrow functionun 1. parametresi VALUE
    arrow functionun 2. parametresi KEY
    arrow functionun 3. parametresi ILGILI MAPIN KENDISI --> myMap


    myMap.forEach((value, key, callingMap)=>{
        DO SOMETHING
    })

    SET
    key and value are BOTH the SAME!!!
*/
const numbers = [2, 4, 6, 7];
numbers.forEach((element, index) =>
  console.log('arr[' + index + ']= ' + element)
);

/* 
    OUTPUT
    key:fname -> value:chandler
    myMap===callingMap true
    key:1 -> value:bing
    myMap===callingMap true
    key:37 -> value:barz
    myMap===callingMap true 
*/
const myMap = new Map([
  ['fname', 'chandler'],
  [1, 'bing'],
  [37, 'barz']
]);

myMap.forEach((value, key, callingMap) => {
  console.log(`key:${key} -> value:${value}`);
  console.log('myMap===callingMap', myMap === callingMap);
});

/* 
    OUTPUT
    key:3 -> value:3
    mySet===callingSet:  true
    key:2 -> value:2
    mySet===callingSet:  true
    key:1 -> value:1
    mySet===callingSet:  true 
*/
const mySet = new Set([3, 2, 1]);
mySet.forEach((value, key, callingSet) => {
  console.log(`key:${key} -> value:${value}`);
  console.log('mySet===callingSet: ', mySet === callingSet);
});

/*
    OUTPUT
    key:3
    key:2
    key:1
*/
mySet.forEach((key, callingSet) => {
  console.log(`key:${key}`);
});
