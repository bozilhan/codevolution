/*
    https://www.youtube.com/watch?v=ol5sgcMvONU&list=PLC3y8-rFHvwhI0V5mE9Vu6Nm-nap8EcjV&index=17
    destructuring --> destroy the structure or dismantle(dagitmak,parcalamak,sokmek) something
    destructuring an array is to pull apart the ELEMENTS of the array
    splitting up this individual elements and assigning them to these VARIABLES
    TAKE THE INDIVIDUAL ELEMENTS IN ARRAY AND ASSIGN THEM TO INDIVIDUAL VARIABLES

    ARRAY DESTRUCTURING
*/
const employee = ['chandler', 'bing', 'male'];

const [fname, lname, gender] = employee;
console.log(fname); // chandler
console.log(lname); // bing
console.log(gender); // male

const employeeII = ['chandler', 'bing'];

const [fname, lname, gender] = employeeII;
console.log(fname); // chandler
console.log(lname); // bing
console.log(gender); // UNDEFINED

const employee = ['chandler', 'bing', 'male'];

/*
    comma comma variableName
*/
const [, , gender] = employee;
console.log(gender); // male

/*
    with rest operator
    since we have a rest operator the remaining values are getting stored into an ARRAY
    and ASSIGNED to elements variable
*/
const [fname, ...elements] = employee;
console.log(fname); // chandler
console.log(elements); // ['bing', 'male']

/*
    we can specify a default value
*/
const employeeII = ['chandler', 'bing'];

const [fname, lname, gender = 'MALE'] = employeeII;
console.log(fname); // chandler
console.log(lname); // bing
console.log(gender); // MALE

const employeeIII = ['chandler', 'bing', 'female'];

const [fname, lname, gender = 'MALE'] = employeeIII;
console.log(fname); // chandler
console.log(lname); // bing
console.log(gender); // female

/*
    https://www.youtube.com/watch?v=kgMglU8gZAo&list=PLC3y8-rFHvwhI0V5mE9Vu6Nm-nap8EcjV&index=18
    
    OBJECT DESTRUCTURING
    use {} INSTEAD OF []
*/

const employeeObject = {
  fname: 'chandler',
  lname: 'bing',
  gender: 'male'
};

/*
    use {} INSTEAD OF []
    !!!we have to ENSURE that the PROPERTY NAME is the SAME as THIS VARIABLE NAME!!!
*/
const { fnam, lnam, gende } = employeeObject;
console.log(fnam); // undefined
console.log(lnam); // undefined
console.log(gende); // undefined

const { fname, lname, gender } = employeeObject;
console.log(fname); // chandler
console.log(lname); // bing
console.log(gender); // male

/*
    create an ALIAS
    we CAN NOT use fname, lname and gender ANYMORE
    we HAVE TO USE the ALIAS
*/
const { fname: f, lname: l, gender: g } = employeeObject;
console.log(f); // chandler
console.log(l); // bing
console.log(g); // male
