/*
    https://www.youtube.com/watch?v=001s2FJ10E8&list=PLC3y8-rFHvwhI0V5mE9Vu6Nm-nap8EcjV&index=19
*/
const user = 'Chandler';
const greet =
  'Welcome \'single_quotes\' "double_quotes" ' + user + ' to es2015';

/*
    backtick `
    ELIMINATE + CHARACTERS
    if you want to use a value that is assigned to a VARIABLE within a string
    we can use $ and wrap the variable name with in CURLY BRACES {}

    greet and greetWithBackTick are EQUIVALENT

    when you use backtick WHITE SPACES are SIGNIFICANT
*/
const greetWithBackTick = `Welcome 'single_quotes' "double quotes" ${user} to es2015`;

console.log(greet);
console.log(greetWithBackTick);

const greetWithBackTickMultiLine = `Welcome 'single_quotes' "double quotes" ${user} to es2015
this is the second line.
Third and so                on`;
console.log(greetWithBackTickMultiLine);
