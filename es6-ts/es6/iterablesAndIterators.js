/*
    https://www.youtube.com/watch?v=c5x7z02Idfo&list=PLC3y8-rFHvwhI0V5mE9Vu6Nm-nap8EcjV&index=38
    iteration --> TRAVERSE DATA

    ITERABLE (array, string, map, set)
    iterable is any object that implements a method whose key is Symbol.iterator

    ITERATOR
    iterator is an object that is going to implement a NEXT method
    this next method KNOWS HOW TO ACCESS ELEMENTS in a COLLECTION
    (array, string, map, set)
    iterator returns an object that is called ITERATOR RESULT OBJECT

    ITERATOR RESULT OBJECT has two properties
    1. value property --> this can be ANY DATA TYPE
       this value is the ACTUAL VALUE within the collection
    2. done property --> this is boolean flag
       indicates if the iteration is COMPLETE or NOT
       if done is true the iteration is COMPLETE
       if done is false the iteration is NOT COMPLETE. There are MORE ELEMENTS to be ITERATED OVER
*/

/*
    CREATE OWN ITERATOR
    iterator must be a FUNCTION

    own for..of loop
    it internally calls next() method UNTIL done property is TRUE
*/

const iterable = [3, 2, 1];

/*
    accept our array as parameter
*/
const createIterator = array => {
  // to iterate or keep track of each element within the array
  let count = 0;

  /*
    iterator contains next() method
    next() method returns the ITERATOR RESULT OBJECT
  */
  return {
    next: () => {
      return count < array.length
        ? {value: array[count++], done: false}
        : {value: undefined, done: true};
    }
  };
};

const myIterator = createIterator(iterable);
console.log(myIterator.next()); // {value: 3, done: false}
console.log(myIterator.next()); // {value: 2, done: false}
console.log(myIterator.next()); // {value: 1, done: false}
console.log(myIterator.next()); // {value: undefined, done: true}

/*
    https://www.youtube.com/watch?v=0kHJgw6Li_4&list=PLC3y8-rFHvwhI0V5mE9Vu6Nm-nap8EcjV&index=39
    PLAIN JS OBJECTS ARE NOT ITERABLE!!!
    so they DONT WORK with the DEFAULT for..of loop
*/

const person = {
  fname: 'chandler',
  lname: 'bing'
};

person[Symbol.iterator] = () => {
  const properties = Object.keys(person);
  let count = 0;
  let isDone = false;
  const next = () => {
    if (count >= properties.length) {
      isDone = true;
    }
    return {done: isDone, value: this[properties[count++]]};
  };

  return {next};
};

/*
  by using GENERATOR
*/

person[Symbol.iterator] = function*() {
  const properties = Object.keys(person);

  for (const t of properties) {
    // yield each key
    //
    yield this[t];
  }
};

for (const p of person) {
  console.log(p);
}

ito