/*
* if you want some piece of code to be used by ONLY WITHIN PARTICULAR MODULE DONT EXPORT IT
* if you want the code to be shared BETWEEN FILES, use module.exports assign it to a variable
* and then in our main file you can use the `required` keyword to import the module and then
* you can call the CORRESPONDING function
*
* it's a good convention to use the name of the variable the same as the name of the module
* */

const relay = () => {
    console.log('This is relay function');
};

const longJump = () => {
    console.log('This is longJump function');
};

/*
* If we want to convert this N005Athletics.js into a module
* */
module.exports.relay = relay;

/*
* ALTERNATIVE module.exports
* */
module.exports = {
    relay: () => {
        console.log('This is relay function');
    },
    longJump: () => {
        console.log('This is longJump function');
    }
};