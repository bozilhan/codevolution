/*
What is Nodejs?
Nodejs is a runtime environment for creating web applications on the server side
Nodejs ALLOWS us to RUN JavaScript ON THE BACKEND OUTSIDE A BROWSER
Nodejs is also LIBRARY because it comes with a lot of built-in modules that saves us a time by not having to code everything from scratch

Why do we use Nodejs?
we have already been using JS for frontend development and by using JS backend as well we are going to be reducing the complexity of building our application
Nodejs is extremely fast because it runs on the google v8 engine and because Nodejs asynchronous event driven model
we can create games chat rooms where multiple users chat simultaneously with with a lot of ease

to run Nodejs application you have to type `node {FILE_NAME}.js`
*/

console.log('Hello world!');
