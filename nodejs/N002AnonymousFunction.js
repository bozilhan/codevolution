/*
* in JS we can also pass functions AS ARGUMENTS TO OTHER FUNCTIONS
*
* how does this work
* the execution begins by calling the mainFunction
*
* functions that have NO NAMES are called anonymous functions
*
* it is also possible to define an anonymous function as a parameter to another function
* */
const printStuff = stuff => {
    console.log(stuff);
};

const mainFunction = (anotherFunction, value) => {
    anotherFunction(value);
};

mainFunction(printStuff, 'hello');


mainFunction(stuff => {
    console.log(stuff);
}, 'barz');