/**
  HTTP module allows us to use the http server and client   

  we can use http module to create our very first WEB SERVER that responds 
  hello world when a request is made from the browser
*/

/**
 * since it http is code module we DONT HAVE TO SPECIFY the dot and the slash
 * which specifies the directory
 */
const http = require('http');

/**
* createServer() returns an object which has a method call to listen and this takes a parameter which is a port number

we create a very first http server and responded with `hello world`

add anonymous function that has two parameters (request, response)

run the file and then go to http://localhost:8888 -> Hello world
*/
http.createServer((request, response) => {
    response.writeHead(200,{'Content-Type':'text/plain'});
    response.write('Hello world');

    // event
    response.end();
}).listen(8888);
