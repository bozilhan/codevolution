What is Nodejs?
Nodejs is a runtime environment for creating web applications on the server side
Nodejs ALLOWS us to RUN JavaScript ON THE BACKEND OUTSIDE A BROWSER
Nodejs is also LIBRARY because it comes with a lot of built-in modules that saves us a time by not having to code everything from scratch

Why do we use Nodejs?
we have already been using JS for frontend development and by using JS backend as well we are going to be reducing the complexity of building our application
Nodejs is extremely fast because it runs on the google v8 engine and because Nodejs asynchronous event driven model
we can create games chat rooms where multiple users chat simultaneously with with a lot of ease

one of the reasons that nodejs is pretty fast is because it is coded around events

to run Nodejs application you have to type `node {FILE_NAME}.js`


https://www.youtube.com/watch?v=ui4-OADfgIk&list=PLC3y8-rFHvwhco_O8PS1iS9xRrdVTvSIz&index=4
callback function as another function that can be passed that is USUALLY PASSED AS AN ARGUMENT TO ANOTHER FUNCTION and it is USUALLY INVOKED AFTER SOME KIND OF AN EVENT