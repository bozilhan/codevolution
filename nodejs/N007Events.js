/**
how to create our own events and then trigger them ourselves  

consider a very basic example 
when you walk into a shop by opening the door 
a tiny bell goes off that  alerts the shopkeeper that he has a customer

so in this case OPENING THE DOOR IS GOING TO BE THE EVENT and  we are going to handle that event by ringing the bell

ringing the bell is actually called an EVENT HANDLER 
what has to HAPPEN WHEN a PARTICULAR EVENT OCCURS
*/
const events = require('events');
const eventEmitter = new events.EventEmitter();

/**
FIRST define WHAT HAS TO HAPPEN WHEN the door is opened  so we need to ring the bell   
*/
const ringBell = () => {
  console.log('Ring ring ring');
};

/**
to specify what has to happen when an event occurs
when this event OCCURS call this function
when 'doorOpen' event OCCURS call ringBell function
*/
eventEmitter.on('doorOpen', ringBell);
eventEmitter.on('bellRings', message => {
  console.log(message);
});

/**
emit(cikarmak,yaymak,gondermek)
we need to trigger that event
EXECUTION STARTS here we are first emitting our door open event
eventEmitter checks through the code and it finds out that when the doorOpen event occurs
we need the call this handler which is a function
'doorOpen' yerine `doorpen` yazsak calismaz!!!

eventEmitter.emit() metodu yazildigi sirada cagriliyor yer degistirirsek output degisir

EXPLANATION
we are going to first omit the event door open so onDoorOpen nodejs knows it is going to help to call ringBell 
Once eventEmitter.emit('doorOpen') is done we are going to say emit `bellRings`
*/
eventEmitter.emit('doorOpen');
eventEmitter.emit('bellRings', 'Welcome');
