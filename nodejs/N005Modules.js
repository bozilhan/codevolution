/*
* to make use of N005Athletics module we will type the variable
* we will make use of the keyword called `require` TO IMPORT MODULES
* and it's going to take a parameter which is the name of the file
*
* since N005Athletics is in the same directory as I've N005Modules.js
* we need to './N005Athletics'
* you can ignore the .js
*
* it's a good convention to use the name of the variable the same as the name of the module
* */

const athletics = require('./N005Athletics');

athletics.relay();