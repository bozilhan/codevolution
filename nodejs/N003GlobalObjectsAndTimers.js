/*
* In nodejs we have objects that can be used directly in application and
* those objects are called GLOBAL OBJECTS and FUNCTIONS
*
* __filename -> this represents the filename of the code being executed
* we have the entire directory to where a file resides and then the name of the file
* C:\workspace\codevolution\codevolution-nodejs\N003GlobalObjectsAndTimers.js
*
* __dirname -> this represents the directory of the file under execution
* C:\workspace\codevolution\codevolution-nodejs
* */
console.log(__filename);
console.log(__dirname);

/*
* setTimeout() accepts two parameters
* the first one is FUNCTION
* the second represents the time in MILLISECONDS
* 5000ms -> 5s
* setTimeout is going to call this function printStuff AFTER 5 SECONDS
* */

const printStuff = () => {
    console.log('This was from setTimeout');
};

setTimeout(printStuff, 5000);

/*
* if you want a function to be executed at REGULAR/PERIODIC INTERVALS of time
* we will use a global function called setInterval
*
* setInterval() accepts two parameters
* the first one is FUNCTION
* the second represents the time in MILLISECONDS
* */
setInterval(printStuff, 2000);

