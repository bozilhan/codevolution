import React, {useState} from 'react';

const UseStateHookCounter = () => {
  /*
   * https://havelsan.udemy.com/course/the-complete-guide-to-advanced-react-patterns/learn/lecture/#content
   * One of the main reasons hooks were added to * React is
   * to offer a more powerful and expressive(anlamli, etkileyici) way
   * to write (and share) functionality between components.
   *
   *
   * hooks basically give FUNCTION COMPONENTS the ability to use local component state
   * execute side-effects and more
   *
   *
   * hooks allows you to use react features without having to write a class
   * state tutamayan functional componentlerin state tutabilmesini saglar
   * class based componentlerin icinde hook KULLANILMAZ
   * 
   * 
   * WHY HOOKS
   * hooks avoid whole confusion with 'this' keyword
   * allow you to reuse stateful logic without changing your component hierarchy. HOC ve render props pattern kullanmak yerine
   * Organize the logic inside a component into reusable isolated units. data fetching subscribing events
   * Mutually related code can be put
   * together which will avoid trivial bugs and inconsistencies
   *
   *
   * 1. import useState
   * 2. call it passing DEFAULT value
   * 3. assign the return of values to variables using array Destructuring
   * 4. use them in the RENDER function
   *
   *
   * very first time (ilk kez) the component renders
   * a state is CREATED and INITIALIZED with the DEFAULT VALUE of 0
   * default value of 0 is NEVER USED on re-renders
   *
   *
   * when you click on the button the setCount method is called
   * which will add one to current count value
   * on first click count is incremented to 1 from 0
   * after that the setCount method will cause the component
   * to RE-RENDER after the re-render count will contain a value of 1
   * which is then displayed as part of the inner text in the browser
   *
   *
   * DONT CALL hooks INSIDE LOOPS, CONDITIONS, OR NESTED FUNCTIONS
   * call them from within REACT FUNCTIONAL COMPONENTS and
   * NOT JUST ANY REGULAR javascript function
   *
   */
  const [count, setCount] = useState(0);

  const onClickCount = () => {
    /*
      it’s also possible to pass a function to the state updater function. 
      This is usually recommended as with class’ setState 
      when a state update depends on a previous value of state 
      e.g. setCount(prevCount => prevCount + 1)
    */
    // setCount(count + 1);
    setCount((previousCount) => previousCount + 1);
  };

  return (
    <div>
      <button onClick={onClickCount}>Count {count}</button>
      {/*<button onClick={() => setCount(count + 1)}>Count {count}</button>*/}
    </div>
  );
};

export default UseStateHookCounter;
