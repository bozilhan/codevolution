import React, {useState} from 'react';

/*
* useState hook kullanirken DAIMA component disinda ilgili tipten degisten yarat
* onu useStatee gec
* */

const UseStateHookCounterPrimitive = () => {
    const initialCount = 0;

    /* using PRIMITIVE as a state variable
    * when the state variable is PRIMITIVE
    *
    * Updating the state with a callback
    * When the new state is calculated using the previous state,
    * you can update the state with a callback
    *    // Toggle a boolean
    *    const [toggled, setToggled] = useState(false);
    *    setToggled(toggled => !toggled);
    *
    *    // Increase a counter
    *    const [count, setCount] = useState(0);
    *    setCount(count => count + 1);
    *
    *    // Add an item to array
    *    const [items, setItems] = useState([]);
    *    setItems(items => [...items, 'New Item']);
    */
    const [count, setCount] = useState(initialCount);

    const onClickReset = () => {
        setCount(initialCount);
    };

    const onClickIncrement = () => {
        /* useState in GUVENILIR kullanim yontemi
        * MEVCUT statee erisen method tanimla
        * ilgili statei al onu update et
        *
        * we pass in FUNCTION that has access to OLD VALUE and
        * INCREMENT that by 1
        */
        setCount(previousCount => previousCount + 1);
    };

    const onClickDecrement = () => {
        setCount(previousCount => previousCount - 1);
    };

    const onClickIncrement5 = () => {
        for (let i = 0; i < 5; i++) {
            /* setCount method is reading a
             * STALE(bayat, eski) value of the count
             * state variable
             *
             * TO OVERCOME this
             * we need to use the second form of setCount function
             * INSTEAD OF PASSING IN VALUE OF THE NEW STATE VARIABLE
             * we pass in function that has access to old state value
             *
             * setCount is going to ACCEPT a FUNCTION that has access to
             * OLD count
             *
             * we pass in FUNCTION that has access to OLD VALUE and
             * INCREMENT that by 1
             *
             * !!!ANYTIME YOU NEED TO UPDATE STATE VALUE BASED ON THE PREVIOUS STATE VALUE
             * ALWAYS GO WITH A SAFER OPTION OF PASSING IN FUNCTION
             * THAT WILL SET THE NEW STATE VALUE!!!
            */
            setCount(previousCount => previousCount + 1);
        }
    };

    return (
        <div>
            Count:{count}
            <button onClick={onClickReset}>Reset {count}</button>
            {/*<button onClick={() => setCount(initialCount)}>Count {count}</button>*/}

            {/*<button onClick={onClickIncrement}>Increment {count}</button>*/}
            <button onClick={() => setCount(previousCount => previousCount + 1)}>Increment {count}</button>

            <button onClick={onClickDecrement}>Decrement {count}</button>
            {/*<button onClick={() => setCount(count - 1)}>Count {count}</button>*/}

            <button onClick={onClickIncrement5}>Increment 5 {count}</button>
            {/*<button onClick={() => setCount(count + 1)}>Count {count}</button>*/}
        </div>
    );
};

export default UseStateHookCounterPrimitive;