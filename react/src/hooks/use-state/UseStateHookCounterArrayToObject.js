import React, {useState} from 'react';

/*
* useState hook kullanirken DAIMA component disinda ilgili tipten degisten yarat
* onu useStatee gec
* */
const UseStateHookCounterArrayToObject = () => {
  /* using ARRAY as a state variable
   * when the state variable is ARRAY
   *
   * the setter function DOESNT AUTOMATICALLY APPEND
   * the item to the end of the list
   * we need to handle that MANUALLY USING SPREAD OPERATOR
   */
  const [items, setItems] = useState([]);

  const onClickAddANumber = () => {
    /* ARRAY of OBJECTS
     *
     * whenever onClickAddANumber is called
     * we make a COPY of ALL THE ITEMS IN THE ARRAY
     * USING THE SPREAD OPERATOR
     *
     * to that list of copied items we simply APPEND another object
     * that way we are not the overwriting the original array
     *
     * on first iteration items is an empty array
     * so id will be 0 which is items.length
     * the value will be a random number between 1 and 10
     *
     * on the next iteration we would have 1 item in the items array
     * so we make a COPY OF THAT and to that we APPEND a NEW OBJECT
     * the id this time will be items.length which is equal to 1 and
     * the value of course will be a random number
     *
     * and the same logic continues for subsequent button clicks
     *
     * MAKE A COPY using SPREAD OPERATOR
     * APPEND THE NEW ITEM AND PASS IT AS ARGUMENT TO SETTER FUNCTION
     * */
    setItems([
      ...items,
      {
        id: items.length,
        /* value random number between 1 and 10 */
        value: Math.floor(Math.random() * 10 + 1)
      }
    ]);
  };

  return (
    <div>
      <button onClick={onClickAddANumber}>Add a number</button>
      <ul>
        {items.map(eachItem => (
          <li key={eachItem.id}>{eachItem.value}</li>
        ))}
      </ul>
    </div>
  );
};

export default UseStateHookCounterArrayToObject;
