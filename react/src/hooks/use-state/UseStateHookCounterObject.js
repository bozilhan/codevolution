import React, {useState} from 'react';

/*
* useState hook kullanirken DAIMA component disinda ilgili tipten degisten yarat
* onu useStatee gec
* */

const UseStateHookCounterObject = () => {
  /* using OBJECT as a state variable
   * when the state variable is OBJECT
   * object will contain firstName and lastName
   * initialize to an empty string
   *
   * useState REPLACES the ENTIRE VALUE
   *
   * the setter method provided by the useState hook
   * DOES NOT AUTOMATICALLY MERGE and UPDATE OBJECTS
   * you have to MANUALLY MERGE it YOURSELF and
   * then PASS the VALUE to the setter method
   */
  const [name, setName] = useState({firstName: '', lastName: ''});

  const onChangeLastName = (e) => {
    /* objectin mevcut degerini SPREAD operator ile aldik
     * icerisindeki bir key,value pairi guncelledik
     *
     * eger spread operatorle objectin mevcut halini almasaydik
     * HER GUNCELLEMEDE ilgili objectin TUM STATEI SILINIR ve
     * SADECE igili key,value pair guncellenirdi.
     *
     * Baska metodda ilgili objectin diger bir key,value pairi guncellenseydi de
     * buradaki key,value pairi kaybederdik
     *
     * KAFANDA CANLANMADIYSA ...name, u sil oyle dene!!!
     *
     *
     * {...name, lastName: e.target.value} --> COPY EVERY PROPERTY in the name object
     * and then JUST OVERRIDE(ONLY UPDATE) the lastName field with a DIFFERENT value!!!
     *
     *
     * SPREAD OPERATOR --> COPY EVERY PROPERTY in the object
     */
    setName({...name, lastName: e.target.value});
  };

  return (
    <form>
      <input
        type='text'
        value={name.firstName}
        onChange={(e) => setName({...name, firstName: e.target.value})}
      />
      <input type='text' value={name.lastName} onChange={onChangeLastName} />
      <h2>Your first name is - {name.firstName}</h2>
      <h2>Your last name is - {name.lastName}</h2>
      <h2>{JSON.stringify(name)}</h2>
    </form>
  );
};

export default UseStateHookCounterObject;
