import React, {useState} from 'react';
/*
* useState hook kullanirken DAIMA component disinda ilgili tipten degisten yarat
* onu useStatee gec
* */
const initialItems = [];

const UseStateHookCounterArrayToString = () => {
  const [items, setItems] = useState(initialItems);
  const [itm, setItm] = useState(0);

  const onClickAddANumber = () => setItems(prevState => [...prevState, itm]);

  const onChangeItem = e => setItm(e.target.value);

  return (
    <>
      <input type='text' onChange={onChangeItem} />
      <button onClick={onClickAddANumber}>Add a number</button>
      <ul>
        {items.map((item, index) => (
          <li key={index}>{item}</li>
        ))}
      </ul>
    </>
  );
};

export default UseStateHookCounterArrayToString;
