import React, {useEffect, useRef} from 'react';
/*
* useRef hook makes it possible to access DOM nodes directly
* within functional components
*
* https://www.youtube.com/watch?v=yCS2m01bQ6w&list=PLC3y8-rFHvwisvxhZ135pogtX7_Oe3Q3A&index=28
*/
const UseRefHookFocusInput = () => {
    const inputRef = useRef(null);
    useEffect(
        () => {
            // focus the input element
            inputRef.current.focus();
        }, []
    );

    return (
        <div>
            <input ref={inputRef}
                   type="text"/>
        </div>
    );
};

export default UseRefHookFocusInput;