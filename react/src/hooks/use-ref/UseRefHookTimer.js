import React, {useEffect, useRef, useState} from 'react';

/*
* https://www.youtube.com/watch?v=LWg0OyZQffc&list=PLC3y8-rFHvwisvxhZ135pogtX7_Oe3Q3A&index=29
*/
const UseRefHookTimer = () => {
    const [timer, setTimer] = useState(0);
    const intervalRef = useRef();


    useEffect(() => {
            intervalRef.current = setInterval(() => {
                setTimer(prevTimer => prevTimer + 1);
            }, 1000);

            return () => {
                clearInterval(intervalRef.current);
            };
        }, []
    );

    return (
        <div>
            Hook Timer - {timer}
            <button onClick={() => clearInterval(intervalRef.current)}>Clear Hook Timer</button>
        </div>
    );
};

export default UseRefHookTimer;