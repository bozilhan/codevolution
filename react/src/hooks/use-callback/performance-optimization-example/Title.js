import React from 'react';

const Title = () => {
    console.log('Rendering Title');
    return (
        <h2>
            useCallback hook
        </h2>
    );
};

/*
* Your component will now re-render
* ONLY IF THERE IS A CHANGE IN ITS PROPS or STATE
*/
export default React.memo(Title);