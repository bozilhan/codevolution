import React from 'react';

/*
* props.children refers to 'Increment Age' and 'Increment Salary'
* text in ParentComponent
*/
const Button = ({handleClick, children}) => {
    console.log('Rendering Button - ', children);
    return (
        <button onClick={handleClick}>
            {children}
        </button>
    );
};

export default React.memo(Button);