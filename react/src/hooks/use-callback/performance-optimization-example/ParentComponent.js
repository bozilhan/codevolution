import React, {useCallback, useState} from 'react';
import Count from './Count';
import Title from './Title';
import Button from './Button';
/*
* React.memo is higher-order component
* that will prevent a functional component from being
* RE-RENDER if it's props or state do not change
*
* https://www.youtube.com/watch?v=IL82CzlaCys&list=PLC3y8-rFHvwisvxhZ135pogtX7_Oe3Q3A&index=26
*
* What?
* useCallback is a hook that will return a MEMOIZED version of the
* callback function that only changes IF ONE OF THE DEPENDENCIES HAS CHANGED
*
* Why?
* It is useful when passing callbacks to optimized child components
* that rely on reference equality to prevent unnecessary renders
*/
const ParentComponent = () => {
    const [age, setAge] = useState(25);
    const [salary, setSalary] = useState(50000);

    const handleIncrementAge = useCallback(
        () => {
            setAge(prevState => prevState + 1);
        }, [age]
    );

    const handleIncrementSalary = useCallback(
        () => {
            setSalary(prevState => prevState + 1000);
        }, [salary]
    );

    return (
        <div>
            <Title/>
            <Count text="Age" count={age}/>
            <Button handleClick={handleIncrementAge}>Increment Age</Button>
            <Count text="Salary" count={salary}/>
            <Button handleClick={handleIncrementSalary}>Increment Salary</Button>
        </div>
    );
};

export default ParentComponent;