import {useState} from 'react';

const useInput = (initialValue) => {
    /*
    * create the state variable which basically tracks the input fields value
    */
    const [value, setValue] = useState(initialValue);

    const reset = () => {
        setValue(initialValue);
    };

    const bind = {
        value,
        onChange: e => {
            setValue(e.target.value);
        }
    };
    
    return [value, bind, reset];
};

export default useInput;