import React from 'react';
import useInput from './useInput';

const UserFormWithUseInputCustomHook = () => {
    const initialValue = '';

    const [firstName, bindFirstName, resetFirstName] = useInput(initialValue);
    const [lastName, bindLastName, resetLastName] = useInput(initialValue);

    const submitHandler = e => {
        // to stop the page refreshing
        e.preventDefault();
        alert(`Hello ${firstName} ${lastName}`);
        resetFirstName();
        resetLastName();
    };

    return (
        <div>
            <form onSubmit={submitHandler}>
                <div>
                    <label>First Name</label>
                    <input
                        {...bindFirstName}
                        type="text"
                    />
                </div>
                <div>
                    <label>Last Name</label>
                    <input
                        {...bindLastName}
                        type="text"
                    />
                </div>
                <button>Submit</button>
            </form>
        </div>
    );
};

export default UserFormWithUseInputCustomHook;