import {useState} from 'react';

const useCounter = (initialCount = 0, value) => {
  const [count, setCount] = useState(initialCount);

  const increment = () => {
    setCount((previousCount) => previousCount + value);
  };

  const decrement = () => {
    setCount((previousCount) => previousCount - value);
  };

  const reset = () => {
    setCount(initialCount);
  };

  /*
   * its name MUST BEGIN with the word use
   *
   * if needed, it may call any of the React hooks within itself.
   *
   * our custom hook return an ARRAY OF VALUES
   *
   * by doing this we can now access the VALUE and METHODS
   * using array destructuring in the CLIENT COMPONENT
   */
  return [count, increment, decrement, reset];
};

export default useCounter;
