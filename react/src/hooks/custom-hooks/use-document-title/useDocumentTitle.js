import {useEffect} from 'react';
/*
* React i import etmeye GEREK YOK. cunku JSX kodu yok return blogu SILINDI
* function name for a custom hook SHOULD START with the word 'use'
*/
const UseDocumentTitle = (count) => {
    useEffect(
        () => {
            document.title = `Count ${count}`;
        }, [count]
    );

};

export default UseDocumentTitle;