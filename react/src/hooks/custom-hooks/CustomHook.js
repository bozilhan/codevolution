import React from 'react';
/*
* A custom hook is basically a javascript function whose NAME STARTS WITH 'use'
* A custom hook can also call other hooks if required
*
* Why?
* TO SHARE LOGIC BETWEEN TWO OR MORE COMPONENTS
* Alternative to HOCs and Render Props Pattern
*
* ayni isi yapan bir komponenti 10 kez klonlamak/kopyalamak yerine
* CUSTOM HOOK KULLANILIR
*/
const CustomHook = () => {
    return (
        <div>

        </div>
    );
};

export default CustomHook;