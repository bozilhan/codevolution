import React, {useEffect, useState} from 'react';

const UseEffectHookOnlyOnce = () => {
    /*
     * useEffect SADECE BIR KEZ tetikleniyor
     * class componentdeki componentDidMount implement ediyoruz
     *
    */

    const [x, setX] = useState(0);
    const [y, setY] = useState(0);

    const logMousePosition = (e) => {
        console.log('UseEffectHookOnlyOnce Mouse event');
        setX(e.clientX);
        setY(e.clientY);
    };

    /*
     * we dont really want the effect to depend on ANYTHING
     * we want useEffect to be called ONCE on INITIAL RENDER ONLY
     * the way we achiever that is by simply specifying
     * an EMPTY DEPENDENCY ARRAY
     *
     * we are basically telling react hey this particular useEffect
     * does NOT DEPEND ON ANY PROPS, STATE OR METHOD
     * so there is just NO REASON for you to CALL this useEffect
     * on RE-RENDERS
     *
     * and react replies so you want me to call this useEffect
     * ONLY INITIAL RENDER
     *
    */
    useEffect(
        // FUNCTION
        () => {
            // mouse event listener
            console.log('UseEffectHookOnlyOnce useEffect called');
            window.addEventListener('mousemove', logMousePosition);

            /* Ilgili componenti resource olarak alan
            * client componentte MEMORY LEAK olusmasini engellemek icin
            * cleanup metodu ekliyoruz.
            *
            * CLEANUP code can be cancelling
            * SUBSCRIPTIONS, TIMERS or even removing EVENT HANDLERS
            * */
            return () => {
                console.log('UseEffectHookOnlyOnce component unmounted');
                window.removeEventListener('mousemove', logMousePosition);
            };

        }, []
    );

    return (
        <div>
            Hooks X - {x} Y - {y}
        </div>
    );
};

export default UseEffectHookOnlyOnce;