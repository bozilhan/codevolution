import React, {useEffect, useState} from 'react';
/*
 * https://www.youtube.com/watch?v=SP-NrbQHFww&list=PLC3y8-rFHvwisvxhZ135pogtX7_Oe3Q3A&index=11
*/
const UseEffectHookWithIncorrectDependency = () => {
    const [count, setCount] = useState(0);

    const tick = () => {
        setCount(prevState => prevState + 1);
        // asagidaki formu kullanirsak timer da surekli 1 gorunur
        // setCount(count + 1);
    };


    useEffect(() => {
            /*
             * whenever you need to call function with in useEffect just
             * go ahead and define the function with in the useEffect
             *
             * boylelikle ilgili metodun kullandigi
             * local degiskenleri
             * DEPENDENCY ARRAYe ekleyebiliriz
             *
             * function doSomething() {
                console.log('doSomething');
             * }
             *
            */
            const doSomething = () => {
                console.log('doSomething');
            };

            doSomething();

            // call tick method every second 1000ms
            const interval = setInterval(tick, 1000);

            //cleanup
            return () => {
                clearInterval(interval);
            };

        }, []
    );

    return (
        <div>
            {count}
        </div>
    );
};

export default UseEffectHookWithIncorrectDependency;