import React, {useState} from 'react';
import UseEffectHookOnlyOnce from './UseEffectHookOnlyOnce';

const UseEffectHookWithCleanup = () => {
    /*
     * useEffect ile
     * class componentdeki componentWillUnmount u implement ediyoruz
     *
     *
     * WHY we need componentWillUnmount
     *
     *
     * cleanup MEMORY LEAK hatasi icin gerekli
     *
     * very first time i'm going to click on the toggle display button
     * this will effectively UNMOUNT UseEffectHookOnlyOnce component
     * from the DOM now when I try to move the mouse around
     * we can see a warning in the console
     *
     * index.js:1 Warning: Can't perform a React state update on an unmounted component.
     * This is a no-op, but it indicates a memory leak in your application.
     * To fix, cancel all subscriptions and asynchronous tasks in a useEffect cleanup function.
     * in UseEffectHookOnlyOnce (at UseEffectHookWithCleanup.js:22)
     *
     * below the warning we can see that the statement mouse event
     * is still being logged
     *
     * EVEN THOUGH the component has been removed the EVENT LISTENER which belongs to that componenr
     * is STILL LISTENING and EXECUTING
     *
     * THIS IS WHAT THE WARNING INDICATES AS WELL
     * React is telling us hey you have unmounted the component
     * but when you move your mouse around you are asking me to update
     * the state variables for x and y coordinates
     * the only problem is that component has been unmounted
     *
     * WHEN YOU UNMOUNT A COMPONENT MAKE SURE YOU CANCEL ALL YOUR SUBSCRIPTIONS AND LISTENIERS
     * in other words CLEANUP AFTER your component
     * by
    */

    const [display, setDisplay] = useState(true);

    // {} olmadan kullandik
    const onClickToggle = () => setDisplay(prevState => !prevState);

    return (
        <div>
            <button onClick={onClickToggle}>Toggle Display</button>
            {display && <UseEffectHookOnlyOnce/>}
        </div>
    );
};

export default UseEffectHookWithCleanup;