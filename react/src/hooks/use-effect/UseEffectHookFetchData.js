import React, {useEffect, useState} from 'react';
import axios from 'axios';

const UseEffectHookFetchData = () => {
    /*
    * How to fetch data from an API end point
    * https://www.youtube.com/watch?v=bYFYF2GnMy8&list=PLC3y8-rFHvwisvxhZ135pogtX7_Oe3Q3A&index=12
    * */

    const [posts, setPosts] = useState([]);
    const [post, setPost] = useState({});
    const [id, setId] = useState(1);
    const [idFromButtonClick, setIdFromButtonClick] = useState(1);

    // create useEffect to fetch the data from URL endpoint
    useEffect(() => {
            // we make our GET request
            axios.get(`https://jsonplaceholder.typicode.com/posts/${id}`)
                .then(res => {
                    // res --> response returned from promise
                    console.log(res);
                    setPost(res.data);
                })
                .catch(err => {
                    // error
                    console.log(err);
                });

            // DEPENDENCY arraye id eklemezsek
            // inputa hangi idyi girersek girelim
            // SADECE id:1 e ait baslik gorunur
            // text box dan id yi silersek bu get istegi TUM KAYITLARI DONER
            // fakat biz bu durumu burada yazdigimiz koddan dolayi goz ardi ettik
        }, [id]
    );

    // buttona clikleyince text fieldin icindeki degeri alip ilgili id yi get etme
    useEffect(() => {
            // we make our GET request
            axios.get(`https://jsonplaceholder.typicode.com/posts/${idFromButtonClick}`)
                .then(res => {
                    // res --> response returned from promise
                    console.log(res);
                    setPost(res.data);
                })
                .catch(err => {
                    // error
                    console.log(err);
                });

        }, [idFromButtonClick]
    );

    useEffect(() => {
            // we make our GET request
            axios.get('https://jsonplaceholder.typicode.com/posts')
                .then(res => {
                    // res --> response returned from promise
                    console.log(res);
                    setPosts(res.data);
                })
                .catch(err => {
                    // error
                    console.log(err);
                });

            // empty dependency list KOYMAZSAK INFINITE LOOP
        }, []
    );

    const onChangeInput = (e) => {
        setId(e.target.value);
    };

    const onClickFetchPost = () => {
        // id value which is the text input field value
        setIdFromButtonClick(id);
    };

    return (
        <div>
            <input
                type="text"
                value={id}
                onChange={onChangeInput}
            />
            <button
                type="button"
                onClick={onClickFetchPost}
            >
                Fetch Post
            </button>

            <div>{post.title}</div>

            <ul>
                {posts.map(forEachPost =>
                    <li key={forEachPost.id}>
                        {forEachPost.title}
                    </li>)}
            </ul>
        </div>
    );
};

export default UseEffectHookFetchData;