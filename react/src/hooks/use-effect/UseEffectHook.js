import React, {useEffect, useState} from 'react';

const UseEffectHook = () => {
  const [count, setCount] = useState(0);
  const [name, setName] = useState('');

  /*
    * https://stackoverflow.com/questions/55265604/uncaught-invariant-violation-too-many-re-renders-react-limits-the-number-of-re
    * just like useState useEffect is also a function
    * by default will be called FIRST on MOUNT and 
    * subsequently run on a rate dependency within it changes.
    *
    * when we specify useEffect we are basically requesting react
    * to EXECUTE the function THAT IS PASSED AS AN ARGUMENT
    * EVERY TIME THE COMPONENT RENDERS!!!
    *
    * useEffect is created for LOGGING, FETCHING DATA or MANAGING SUBSCRIPTIONS
    * useEffect is called by passing FUNCTION within which you can PERFORM 
    * logging, fetching data or managing subscriptions
    * 
    * useEffect will be invoked 
    * when the functional component MOUNTS and
    * when the functional component UPDATES
    *
    * https://www.youtube.com/watch?v=8DYlzVUTC7s&list=PLC3y8-rFHvwisvxhZ135pogtX7_Oe3Q3A&index=8
    * in some cases applying the useEffect every render
    * might create a PERFORMANCE PROBLEM
    *

    * [] --> PARAMETER ARRAY/DEPENDENCY ARRAY
    * tell the react to CONDITIONALLY RUN useEffect
    * only when the SPECIFIC variable CHANGES
    * within this array we need to SPECIFY either
    * props, state, method that we NEED TO WATCH for!!!
    *
    * Only if those props, states or methods SPECIFIED in this array
    * were to change the effect would be EXECUTED
    *
    * We are CONDITIONALLY update the title
    * ONLY when the APPROPRIATE VARIABLE/METHOD changes
    *
    * Bu ornekte count degiskeni dependency arraye eklendiginden
    * input alanindaki her tus basma useEfeect i TETIKLEMEZ,
    * SAYFA RE-RENDER GERCEKLESMEZ!!!
    *
    * useEffect sadece count un statei degistiginde degisir
    * SADECE count DEGISTIGINDE RE-RENDER GERCEKLESIR!!!
    *
    * if the values included in dependency array
    * DONT CHANGE BETWEEN RENDERS useEffect is NOT RUN!!!
    */
  useEffect(
    /*
     * this parameter is a function
     * which GETS EXECUTED AFTER EVERY RENDER
     * of the component
     */
    () => {
      console.log('useEffect -  Updating document title');
      console.log('useEffect -  name:', name);
      document.title = `You clicked ${count} times`;
    },
    /*
        dependencies
        If you pass any values into this array, 
        then the effect function will be run on mount, 
        and anytime the values passed are updated. 
        i.e if any of the values are changed, 
        the effected call will re-run.

        dependency array bossa useEffect SADECE BIR KEZ MOUNT SIRASINDA CAGRILIR

        dependency arrayde kac tane eleman varsa useEffect bir kez mount sirasinda cagrilmasinin yanisira
        LISTEDE YER ALAN HER BIR ELEMANIN GUNCELLENMESINDE DE CAGRILIR

        useEffect SONSUZ DONGUYE GIREBILIR dolayisiyla dependency array elemanlarini DIKKATLI BELIRLEMEK GEREKIR

        Tanimlayacagimiz herhangi bir useEffectin ISTEDIGIMIZ SEKILDE calistigindan EMIN OLANA KADAR LOGLAYALIM


        The effect function will be run on mount, and whenever the count function changes.
        count changes when the button is clicked, so the effect function re-runs
    */
    [count]
  );

  const onClickCount = () => {
    setCount((previousCount) => previousCount + 1);
  };

  const onChangeName = (e) => {
    /*
     * input alanindan deger boyle aliniyor
     * HATA VERIR
     * setName(name => e.target.value);
     * setName(name => name + e.target.value);
     * setName(prevState => prevState + e.target.value);
     * HATA VERIR
     */
    setName(e.target.value);
  };

  return (
    <div>
      <input type='text' value={name} onChange={onChangeName} />
      <button onClick={onClickCount}>Count {count}</button>
    </div>
  );
};

export default UseEffectHook;
