import React, {useMemo, useState} from 'react';

const UseMemoHookCounter = () => {
    const [counterOne, setCounterOne] = useState(0);
    const [counterTwo, setCounterTwo] = useState(0);

    const incrementOne = () => {
        setCounterOne(prevState => prevState + 1);
    };

    const incrementTwo = () => {
        setCounterTwo(prevState => prevState + 1);
    };

    /*
    * every time the state updates the component re-renders
    * when the component re-renders these even function is called
    * again the function is slow and hence even when we update counter
    * to the UI update is slow
    *
    * so what we need is a way to tell react not to recalculate certain
    * values when unnecessary especially the ones
    * WHICH TAKE A LONG TIME to compute
    *
    * in our example we need to tell react not to calculate whether counterOne
    * is odd or even when we are changing counterTwo values
    *
    * this is where the useMemo hook comes into picture
    *
    * useMemo is a hook that will only recompute the cached value when
    * one of the dependencies has changed
    *
    * this optimization heads to avoid expensive calculations on every render
    *
    * useMemo returns the cached value which we are going to assign to the variable
    * isEven
    *
    * isEven is not going to be a function call because it now stores a value
    *
    * useCallback and useMemo are very SIMILAR what is the difference
    * useCallback caches PROVIDED FUNCTION INSTANCE ITSELF
    * whereas useMemo INVOKES the PROVIDED FUNCTION and CACHES ITS RESULT
    *
    * so if you need to CACHE A FUNCTION USE useCallback
    * when you need to CACHE THE RESULT OF INVOKED FUNCTION USE useMemo
    */
    const isEven = useMemo(() => {
        // loop doesnt really effect a return value
        // but it does slow down the rate at which we compute
        let i = 0;
        while (i < 2000000000) {
            i++;
        }
        return (counterOne & 1) === 0;
    }, [counterOne]);

    return (
        <div>
            <div>
                <button onClick={incrementOne}>Count One - {counterOne}</button>
                <span>{isEven ? 'Even' : 'Odd'}</span>
            </div>
            <div>
                <button onClick={incrementTwo}>Count Two - {counterTwo}</button>
            </div>
        </div>
    );
};

export default UseMemoHookCounter;