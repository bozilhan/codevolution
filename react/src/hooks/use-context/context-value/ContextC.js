import React from 'react';
import ContextE from './ContexE';
import UseContextHookComponentE from '../UseContextHookComponentE';

const ContextC = () => {
    /*
    * https://www.youtube.com/watch?v=CI7EYWmRDJE&list=PLC3y8-rFHvwisvxhZ135pogtX7_Oe3Q3A&index=15
    *
    * Context provides a way to pass data through the component tree
    * WITHOUT HAVING TO PASS PROPS DOWN MANUALLY AT EVERY LEVEL
    *
    *  CONTEXT
    *  our goal is to pass the username prop from App component
    *  add read that prop in ContextF using context-value
    *  there are 3 steps to implement making use of context-value
    *  1. Create the context-value we can create it in the App Component
    *  2. we need to provide this context-value with a value and
    *     provider must wrap the children components for the value to be available
    *     so we wrap ContextC. ContextC is going to be a child to this provider
    *  3. Consume the context-value using the RENDER PROPS PATTERN.
    *     So from App component let us export the context-value and in
    *     ContextF let us import it
    */
    return (
        <div>
            <ContextE/>
            <UseContextHookComponentE/>
        </div>
    );
};

export default ContextC;