import React, {useContext} from 'react';
import {ChannelContext, UserContext} from '../../App';

const UseContextHookComponentE = () => {
    /*
    *  1. ve 2. adimlar App.js de
    * useContext Hook
    * useContext hook only makes the CONSUMPTION of the context value SIMPLER
    *  there are 3 steps to implement making use of context-value
    *  1. Create the context-value we can create it in the App Component
    *  2. we need to provide this context-value with a value and
    *     provider must wrap the children components for the value to be available
    *     so we wrap ContextC. ContextC is going to be a child to this provider
    *  3. There are three simple steps to consume the context value
    *   3.1. import useContext from react
    *   3.2. import necessary context
    *   3.3. call the useContext function
    *
    *
    * useContext returns the context value
    */
    const user = useContext(UserContext);
    const channel = useContext(ChannelContext);

    return (
        <div>
            {user}-{channel}
        </div>
    );
};

export default UseContextHookComponentE;