import React, {useEffect, useState} from 'react';
import axios from 'axios';
// https://www.youtube.com/watch?v=snzS7-73SEQ&list=PLC3y8-rFHvwisvxhZ135pogtX7_Oe3Q3A&index=23&pbjreload=10
const UseReducerHookFetchDataOne = () => {
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState('');
    const [post, setPost] = useState({});

    useEffect(
        () => {
            axios.get('https://jsonplaceholder.typicode.com/posts/1')
                .then(response => {
                    setLoading(false);
                    setPost(response.data);
                    setError('');
                })
                .catch(error => {
                    setLoading(false);
                    setPost({});
                    setError('Something went wrong');
                });

        }, []
    );

    return (
        <div>
            {loading ? 'Loading' : post.title}
            {error ? error : null}
        </div>
    );
};

export default UseReducerHookFetchDataOne;