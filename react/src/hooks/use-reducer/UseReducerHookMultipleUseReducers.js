import React, {useReducer} from 'react';
/*
* when dealing with multiple state variables
* that have the same state transitions 
* it is a GOOD IDEA to have multiple views reducers
* making use of the SAME reducer function
*
* this will avoid the complexity of merging the state
* if it were to be an object and also prevents us from
* duplicating code in the reducer function
*/
const initialState = 0;

const reducer = (state, action) => {
    switch (action) {
        case 'increment':
            return state + 1;
        case 'decrement':
            return state - 1;
        case 'reset':
            return initialState;
        default:
            return state;
    }
};

const UseReducerHookMultipleUseReducers = () => {
    const [count, dispatch] = useReducer(reducer, initialState);
    const [countTwo, dispatchTwo] = useReducer(reducer, initialState);

    const onClickIncrement = () => {
        dispatch('increment');
    };
    const onClickDecrement = () => {
        dispatch('decrement');
    };
    const onClickReset = () => {
        dispatch('reset');
    };

    return (
        <div>
            <div>Count - {count}</div>
            <button onClick={onClickIncrement}>Increment</button>
            <button onClick={onClickDecrement}>Decrement</button>
            <button onClick={onClickReset}>Reset</button>
            <div>
                <div>CountTwo - {countTwo}</div>
                <button onClick={() => dispatchTwo('increment')}>Increment</button>
                <button onClick={() => dispatchTwo('decrement')}>Decrement</button>
                <button onClick={() => dispatchTwo('reset')}>Reset</button>
            </div>
        </div>
    );
};

export default UseReducerHookMultipleUseReducers;