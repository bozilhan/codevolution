import React, {useReducer} from 'react';

/*
* Define reducer function and initialState OUTSIDE THE COMPONENT
*/
const initialState = 0;

/*
* reducer function accepts 2 values and returns 1 value
* two values accepted are the CURRENT STATE and the ACTION
*
* you can think of the action as an instruction to the reducer function
* based on what the action specifies the reducer function performs the
* necessary state transition
*
* reducer function returns the NEW STATE
*
* current stateden new statee gecmek icin ACTION parameter kullanilir
*/
const reducer = (state, action) => {
    switch (action) {
        case 'increment':
            return state + 1;
        case 'decrement':
            return state - 1;
        case 'reset':
            return initialState;
        default:
            return state;
    }
};

const UseReducerHook = () => {
    /*
    * useReducer is a hook that is used for STATE MANAGEMENT
    * useReducer is an ALTERNATIVE to useState
    *
    * useState is built using useReducer
    *
    * useReducer method takes two parameters
    * the first is the REDUCER function
    * the second is an INITIAL STATE that the reducer function can make use of
    *
    * reducer method accepts currentState and action as argument
    * and returns a NEW state
    *
    * action parameter is what DICTATES the state transition
    * from the current state to the new state
    *
    * useReducer hook returns the NEW STATE and
    * DISPATCH METHOD which is basically used to specify the action
    */

    /*
    * useReducer accepts 2 arguments
    * the first one is reducer function
    * the second one is the initial state
    *
    * Define reducer function and initialState OUTSIDE THE COMPONENT
    *
    * dispatch method allows us to execute the code
    * corresponding to particular action
    */
    const [count, dispatch] = useReducer(reducer, initialState);

    const onClickIncrement = () => {
        dispatch('increment');
    };
    const onClickDecrement = () => {
        dispatch('decrement');
    };
    const onClickReset = () => {
        dispatch('reset');
    };

    return (
        <div>
            <div>Count - {count}</div>
            <button onClick={onClickIncrement}>Increment</button>
            <button onClick={onClickDecrement}>Decrement</button>
            <button onClick={onClickReset}>Reset</button>
        </div>
    );
};

export default UseReducerHook;