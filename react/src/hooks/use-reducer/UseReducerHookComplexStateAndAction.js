import React, {useReducer} from 'react';

const initialState = {
    firstCounter: 0,
    secondCounter: 10
};

const reducer = (state, action) => {
    switch (action.type) {
        case 'increment':
            return {...state, firstCounter: state.firstCounter + action.value};
        case 'decrement':
            return {...state, firstCounter: state.firstCounter - action.value};
        case 'increment2':
            return {...state, secondCounter: state.secondCounter + action.value};
        case 'decrement2':
            return {...state, secondCounter: state.secondCounter - action.value};
        case 'reset':
            return initialState;
        default:
            return state;
    }
};

const UseReducerHookComplexStateAndAction = () => {

    const [count, dispatch] = useReducer(reducer, initialState);

    const onClickIncrement = (type, value) => {
        dispatch({type: type, value: value});
    };
    const onClickDecrement = (type, value) => {
        dispatch({type: type, value: value});
    };
    const onClickReset = () => {
        dispatch({type: 'reset'});
    };

    return (
        <div>
            <div>First Counter - {count.firstCounter}</div>
            <div>Second Counter - {count.secondCounter}</div>
            <button onClick={() => onClickIncrement('increment', 1)}>Increment</button>
            <button onClick={() => onClickDecrement('decrement', 1)}>Decrement</button>
            <button onClick={() => onClickIncrement('increment', 5)}>Increment 5</button>
            <button onClick={() => onClickDecrement('decrement', 5)}>Decrement 5</button>
            <div>
                <button onClick={() => onClickIncrement('increment2', 1)}>Increment Counter Two</button>
                <button onClick={() => onClickDecrement('decrement2', 1)}>Decrement Counter Two</button>
            </div>
            <button onClick={onClickReset}>Reset</button>
        </div>
    );
};

export default UseReducerHookComplexStateAndAction;
