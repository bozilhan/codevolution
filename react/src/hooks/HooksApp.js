import React, {useReducer} from 'react';
import UseStateHookCounter from './hooks/use-state/UseStateHookCounter';
import UseStateHookCounterPrimitive from './hooks/use-state/UseStateHookCounterPrimitive';
import UseStateHookCounterObject from './hooks/use-state/UseStateHookCounterObject';
import UseStateHookCounterArray from './hooks/use-state/UseStateHookCounterArray';
import UseEffectHook from './hooks/use-effect/UseEffectHook';
import UseEffectHookWithCleanup from './hooks/use-effect/UseEffectHookWithCleanup';
import UseEffectHookWithIncorrectDependency from './hooks/use-effect/UseEffectHookWithIncorrectDependency';
import UseEffectHookFetchData from './hooks/use-effect/UseEffectHookFetchData';
import ContextC from './hooks/use-context/context-value/ContextC';
import UseReducerHook from './hooks/use-reducer/UseReducerHook';
import UseReducerHookComplexStateAndAction from './hooks/use-reducer/UseReducerHookComplexStateAndAction';
import UseReducerHookMultipleUseReducers from './hooks/use-reducer/UseReducerHookMultipleUseReducers';
import ComponentA from './hooks/use-reducer-with-use-context/ComponentA';
import ComponentC from './hooks/use-reducer-with-use-context/ComponentC';
import ComponentB from './hooks/use-reducer-with-use-context/ComponentB';
import UseReducerHookFetchDataOne from './hooks/use-reducer/UseReducerHookFetchDataOne';
import UseReducerHookFetchDataTwo from './hooks/use-reducer/UseReducerHookFetchDataTwo';
import ParentComponent from './hooks/use-callback/performance-optimization-example/ParentComponent';
import UseMemoHookCounter from './hooks/use-memo/UseMemoHookCounter';
import UseRefHookFocusInput from './hooks/use-ref/UseRefHookFocusInput';
import UseRefHookTimer from './hooks/use-ref/UseRefHookTimer';
import DocTitleOne from './hooks/custom-hooks/use-document-title/DocTitleOne';
import DocTitleTwo from './hooks/custom-hooks/use-document-title/DocTitleTwo';
import CounterOne from './hooks/custom-hooks/use-counter/CounterOne';
import CounterTwo from './hooks/custom-hooks/use-counter/CounterTwo';
import UserForm from './hooks/custom-hooks/use-input/UserForm';
import UserFormWithUseInputCustomHook from './hooks/custom-hooks/use-input/UserFormWithUseInputCustomHook';

/*
*  useContext Hook
*  useContext hook only makes the CONSUMPTION of the context value SIMPLER
*
*  our goal is to pass the username prop from App component
*  add read that prop in ContextF using context-value
*  there are 3 steps to implement making use of context-value
*  1. Create the context-value we can create it in the App Component
*  2. we need to provide this context-value with a value and
*     provider must wrap the children components for the value to be available
*     so we wrap ContextC. ContextC is going to be a child to this provider
*  3. Consume the context-value using the RENDER PROPS PATTERN.
*     So from App component let us export the context-value and in
*     ContextF let us import it
*/

/*
* https://www.youtube.com/watch?v=BCD2irXaVoE&list=PLC3y8-rFHvwisvxhZ135pogtX7_Oe3Q3A&index=22
* useReducer with useContext
* 1. Create a counter in App.js using REDUCER HOOK
* 2. Provide and consume the counter context in the required components
* */

// step2
export const CountContext = React.createContext();

// step1
const initialState = 0;

const reducer = (state, action) => {
    switch (action) {
        case 'increment':
            return state + 1;
        case 'decrement':
            return state - 1;
        case 'reset':
            return initialState;
        default:
            return state;
    }
};

export const UserContext = React.createContext(undefined, undefined);
export const ChannelContext = React.createContext(undefined, undefined);

const HooksApp = () => {
    /*
    * useReducer with useContext
    * we dont want to dispatch any action from App.js
    * we want to be able to dispatch actions from the nested components
    * this is where we begin step 2
    * we make use of context to provide the count value and dispatch method
    * and consume the same from the nested components
    * */
    const [count, dispatch] = useReducer(reducer, initialState);

    return (
        <div>
            <div>
                <UserFormWithUseInputCustomHook/>
            </div>

            <div>
                <UserForm/>
            </div>

            <div>
                <CounterOne/>
                <CounterTwo/>
            </div>

            <div>
                <DocTitleOne/>
                <DocTitleTwo/>
            </div>

            <div>
                <UseRefHookTimer/>
            </div>

            <div>
                <UseRefHookFocusInput/>
            </div>

            <div>
                <UseMemoHookCounter/>
            </div>

            <div>
                <ParentComponent/>
            </div>

            <div>
                <UseReducerHookFetchDataOne/>
            </div>

            <div>
                <UseReducerHookFetchDataTwo/>
            </div>

            <div>
                <CountContext.Provider value={{countState: count, countDispatch: dispatch}}>
                    <div>
                        Count - {count}
                        <ComponentA/>
                        <ComponentB/>
                        <ComponentC/>
                    </div>
                </CountContext.Provider>
            </div>

            <div>
                <UserContext.Provider value="Vishwas">
                    <ChannelContext.Provider value="Codevolution">
                        <ContextC/>
                    </ChannelContext.Provider>
                </UserContext.Provider>
            </div>

            <div>
                <UseReducerHookMultipleUseReducers/>
            </div>

            <div>
                <UseReducerHook/>
            </div>

            <div>
                <UseReducerHookComplexStateAndAction/>
            </div>

            <div>
                <UseStateHookCounter/>
            </div>

            <div>
                <UseStateHookCounterPrimitive/>
            </div>

            <div>
                <UseStateHookCounterObject/>
            </div>

            <div>
                <UseStateHookCounterArray/>
            </div>

            <div>
                <UseEffectHook/>
            </div>

            <div>
                <UseEffectHookWithCleanup/>
            </div>

            <div>
                <UseEffectHookWithIncorrectDependency/>
            </div>

            <div>
                <UseEffectHookFetchData/>
            </div>
        </div>
    );
};

export default HooksApp;





