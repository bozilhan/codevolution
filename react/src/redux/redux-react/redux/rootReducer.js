/*
 * we combine all our reducers
 * and export ONE SINGLE REDUCER
 * that can be passed to the createStore function
 */

import {combineReducers} from 'redux';
import cakeReducer from './cake/cakeReducer';
import iceCreamReducer from './iceCream/iceCreamReducer';
import fetchUsersReducer from './fetchUsers/fetchUsersReducer';
import fetchUserReducer from './fetchUser/fetchUserReducer';

/*
 * keyler react app icerisinde . ile eristigimiz isimler
 */
const rootReducer = combineReducers({
  cake: cakeReducer,
  iceCream: iceCreamReducer,
  users: fetchUsersReducer,
  user: fetchUserReducer
});

export default rootReducer;
