/*
* Define action creator
*
* returns an action that is an object with type property
* */
import {BUY_CAKE} from './cakeTypes';

/*
* (number = 1)
* add a new property to the action
* called PAYLOAD
*/
export const buyCake = (number = 1) => {
    return {
        type: BUY_CAKE,
        payload: number
    };
};