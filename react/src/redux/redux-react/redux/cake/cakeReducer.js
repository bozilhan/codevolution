/*
* Reducer is a function that accepts STATE and ACTION
* as parameters and RETURNS THE NEW STATE
* */

import {BUY_CAKE} from './cakeTypes';

/*
* we start off with 10 cakes
* in the inventory
* */
const initialState = {
    numOfCakes: 10
};

/*
* for state parameter we provide a DEFAULT value of initialState
*/
const cakeReducer = (state = initialState, action) => {
    switch (action.type) {
        case BUY_CAKE:
            return {
                // preserve, make a copy of the state
                ...state,
                // then ONLY CHANGE numOfCakes to state.numOfCakes - action.payload
                numOfCakes: state.numOfCakes - action.payload
            };
        default:
            // return the current state
            return state;
    }
};

export default cakeReducer;