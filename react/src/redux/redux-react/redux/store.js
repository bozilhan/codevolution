/*
* within this file we create our store
* for which we use the createStore method from redux
*/

import {applyMiddleware, createStore} from 'redux';
/*
* redux thunk will allow us to define asynchronous action creators in our application
*/
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import rootReducer from './rootReducer';
import {composeWithDevTools} from 'redux-devtools-extension';

const store = createStore(rootReducer,
    /*
    * our app has 2 middleware
    */
    composeWithDevTools(applyMiddleware(logger, thunk)));

export default store;

/*
* the next step is to provide this store to our react app
* codevolution/src/redux/ReduxApp.js icerisinde
* import {Provider} from 'react-redux';
*/