import {BUY_ICECREAM} from './iceCreamTypes';

const initialState = {
    numOfIceCreams: 20
};

const iceCreamReducer = (state = initialState, action) => {
    switch (action.type) {
        case BUY_ICECREAM:
            return {
                // preserve, make a copy of the state
                ...state,
                // then ONLY CHANGE numOfIceCreams to state.numOfIceCreams - 1
                numOfIceCreams: state.numOfIceCreams - 1
            };
        default:
            // return the current state
            return state;
    }
};

export default iceCreamReducer;