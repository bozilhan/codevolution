/*
* axios which will be used to make get requests to the API end point
*/
import axios from 'axios';

import {FETCH_USERS_FAILURE, FETCH_USERS_REQUEST, FETCH_USERS_SUCCESS} from './fetchUsersTypes';

export const fetchUsersRequest = () => {
    return {
        type: FETCH_USERS_REQUEST
    };
};

export const fetchUsersSuccess = users => {
    return {
        type: FETCH_USERS_SUCCESS,
        payload: users
    };
};

export const fetchUsersFailure = error => {
    return {
        type: FETCH_USERS_FAILURE,
        payload: error
    };
};

/*
* https://www.youtube.com/watch?v=tcCS4mGAq7Q&list=PLC3y8-rFHvwheJHvseC3I0HuYI2f46oAK&index=29
* define ASYNC action creator
*
* making use of the thunk middleware fetchAsyncUsers
* will INSTEAD return ANOTHER FUNCTION
*
* instead of returning an action we return a FUNCTION
* the function doesnt have to be PURE
* it is allowed to have side effects such as async api calls
* the function also receives the DISPATCH METHOD as its ARGUMENT
*/
export const fetchAsyncUsers = () => {
    return (dispatch) => {
        // this will basically set loading key of the state object to true
        dispatch(fetchUsersRequest);

        axios.get('https://jsonplaceholder.typicode.com/users')
            .then(response => {
                // payload of fetchUsersSuccess action
                const users = response.data;

                // when we get the response successfully
                dispatch(fetchUsersSuccess(users));
            })
            .catch(error => {
                // payload of fetchUsersFailure action
                const errorMsg = error.message;
                dispatch(fetchUsersFailure(errorMsg));
            });
    };
};

/*
*     return dispatch => {
        // dispatch();

        // GET
        fetch('https://jsonplaceholder.typicode.com/users')
            .then(response => response.json())
            .then(data => {
                // response.data is the array of users
                const users = response.data.map(eachUser => eachUser.id);

                console.log('\n\n\n\n', data);
                // dispatch();
            })
            .catch(error => {
                console.log('\n\n\n\n', error);
                // dispatch();
            });


        // POST
        fetch('https://jsonplaceholder.typicode.com/users',
            {
                method: 'POST',
                body: JSON.stringify({fakeData: 1}),
                headers: {
                    'Content-type': 'application/json'
                }
            })
            .then(response => response.json())
            .then(data => {
                console.log('\n\n\n\n', data);
                // dispatch();
            })
            .catch(error => {
                console.log('\n\n\n\n', error);
                // dispatch();
            });

        // PUT
        // edit something and then save in on to the db
        fetch(`https://jsonplaceholder.typicode.com/users/${id}`,
            {
                method: 'PUT',
                body: JSON.stringify({fakeData: 200, id: id}),
                headers: {
                    'Content-type': 'application/json'
                }
            })
            .then(response => response.json())
            .then(data => {
                console.log('\n\n\n\n', data);
                // dispatch();
            })
            .catch(error => {
                console.log('\n\n\n\n', error);
                // dispatch();
            });


        // DELETE
        // edit something and then save in on to the db
        fetch(`https://jsonplaceholder.typicode.com/users/${id}`,
            {
                method: 'DELETE'
            }
        )
            .then(response => response.json())
            .then(data => {
                console.log('\n\n\n\n', data);
                // dispatch();
            })
            .catch(error => {
                console.log('\n\n\n\n', error);
                // dispatch();
            });
    };
* */