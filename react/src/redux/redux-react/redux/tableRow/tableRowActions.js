import {TR_SET} from './tableRowTypes';


export const setTableRow = (id, data) => ({
    type: TR_SET,
    payload: {
        id: id,
        data: data
    }
});
