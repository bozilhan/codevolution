import {TR_SET} from './tableRowTypes';


const initialState = {
    id: -1,
    data: {}
};

const tableRowReducer = (state = initialState, action) => {
    switch (action.type) {
        case TR_SET:
            return {
                ...state,
                id: action.payload.id,
                data: action.payload.data
            };
        
        default:
            return state;
    }
};

export default tableRowReducer;