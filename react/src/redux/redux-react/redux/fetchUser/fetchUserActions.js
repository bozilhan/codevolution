import {FETCH_USER_FAILURE, FETCH_USER_REQUEST, FETCH_USER_RESPONSE, FETCH_USER_SUCCESS} from './fetchUserTypes';

export const fetchUserRequest = () => {
    return {
        type: FETCH_USER_REQUEST
    };
};

export const fetchUserResponse = response => {
    return {
        type: FETCH_USER_RESPONSE,
        payload: response
    };
};

export const fetchUserSuccess = user => {
    return {
        type: FETCH_USER_SUCCESS,
        payload: user
    };
};

export const fetchUserFailure = error => {
    return {
        type: FETCH_USER_FAILURE,
        payload: error
    };
};

export const fetchUser = id => {
    return dispatch => {
        dispatch(fetchUserRequest);

        console.log('fetchUser: ', id);

        fetch(`https://jsonplaceholder.typicode.com/users/${id}`)
            .then(response => response.json())
            .then(data => {
                console.log('\n\n\n\n', data);
                dispatch(fetchUserSuccess(data));
            })
            .catch(error => {
                dispatch(fetchUserFailure(error));
            });
    };
};
