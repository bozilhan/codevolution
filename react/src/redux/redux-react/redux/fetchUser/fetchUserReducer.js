import {FETCH_USER_FAILURE, FETCH_USER_REQUEST, FETCH_USER_RESPONSE, FETCH_USER_SUCCESS} from './fetchUserTypes';

const initialState = {
    loading: false,
    user: {},
    response: {},
    error: ''
};

const fetchUserReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_USER_REQUEST:
            return {
                ...state,
                loading: true
            };
        case FETCH_USER_RESPONSE:
            return {
                loading: false,
                user: {},
                response: action.payload,
                error: ''
            };
        case FETCH_USER_SUCCESS:
            return {
                loading: false,
                user: action.payload,
                response: {},
                error: ''
            };
        case FETCH_USER_FAILURE:
            return {
                loading: false,
                user: {},
                response: {},
                error: ''
            };
        default:
            return state;
    }
};

export default fetchUserReducer;