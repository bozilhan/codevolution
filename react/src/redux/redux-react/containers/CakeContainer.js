import React from 'react';
import {connect} from 'react-redux';
import {buyCake} from '../redux';

/*
* SCENARIO
* we start off(yola cikmak, koyulmak, baslamak) with our APPLICATION
* which is now a SIMPLE REACT application
*
* the state of the app is maintained SEPARATELY in the Redux Store
*
* our app is ALWAYS SUBSCRIBED to this redux store
* !!!HOWEVER the app CANNOT DIRECTLY UPDATE the STATE!!!
*
* if the app wants to update the state it has to DISPATCH(gondermek, yollamak,sevketmek) an action
* once(-ir -mez) an action has been dispatched the REDUCER than handles(islemek, ele almak) that action
* and UPDATES the CURRENT state
*
* as soon as(hemen, -ir -mez, -ince) the state is updated the value is than passed on to
* the app because the app is subscribed to store
*
* https://www.youtube.com/watch?v=g5l8xu6i4XQ&list=PLC3y8-rFHvwheJHvseC3I0HuYI2f46oAK&index=15
* FOLDER STRUCTURE
* codevolution/src/redux/redux-react/redux
* everything to do WITH redux will be contained in this PARTICULAR FOLDER
* next create folder by FEATURE for our cakeShop application we just have ONE FEATURE --> cakes
*/

/*
* CONNECT
* https://www.youtube.com/watch?v=gFZiQnM3Is4&list=PLC3y8-rFHvwheJHvseC3I0HuYI2f46oAK&index=18
* how do we get hold of the redux state and how do we dispatch an action from within
*
* 1. define a new function mapStateToProps
* 2. define a new function mapDispatchToProps
* 3. we are going to connect these two functions with our react component
*    for that we use the connect function import {connect} from 'react-redux';

* const CakeContainer = (props) => {
        <div>
            <h2>Number of cakes - {props.numOfCakes}</h2>
            <button onClick={props.buyCake}>Buy Cake</button>
        </div>
*/

/*
* numOfCakes from mapStateToProps
* buyCake from mapDispatchToProps
*
* HOW IT ALL WORKS
* first is mapStateToProps function
* when you want to access the redux state in your component you DEFINE mapStateToProps function
* it gets a redux state as a parameter which can be used to retrieve(geri almak) the APPROPRIATE STATE PROPERTIES
* in our case we map state.numOfCakes to a prop called numOfCakes which will then render in the JSX
*
* similarly for dispatching actions we have mapDispatchToProps function
* this function gets the dispatch method as a parameter and allows us
* to map ACTION CREATORS to props in our component
* in our example we map dispatching buyCake to a prop called buyCake
* this allows us to call buyCake as props.buyCake
*
* all this is possible BECAUSE OF the CONNECT FUNCTION from react-redux
* the CONNECT function connects a react component to the REDUX-STORE
* in our case CONNECT function connects CakeContainer to the REDUX-STORE
*
* in react-redux 7.1 hooks have been added so react-redux now offers
* a set of hook APIs as an ALTERNATIVE to existing CONNECT hoc
* the APIs allow you to subscribe to the redux store and dispatch actions
* WITHOUT having to wrap your components and connect
*/
const CakeContainer = ({numOfCakes,buyCake}) => {
    return (
        <div>
            <h2>Number of cakes - {numOfCakes}</h2>
            <button onClick={buyCake}>Buy Cake</button>
        </div>
    );
};

/*
* this function gets a redux state as a parameter and
* returns an object
*
* the state from the redux store is MAPPED to our component PROPS
* it REFLECTS numOfCakes in the redux store
*/
const mapStateToProps = state => {
    return {
        numOfCakes: state.cake.numOfCakes
    };
};

/*
* this function gets a redux dispatch as a parameter and
* returns an object
*
* map our dispatch of an action creator to a PROP in our component
* our component now receives a 2nd additional prop called buyCake
* which will basically dispatch the buy cake action
*
* the second parameter is basically props
* that have already been passed to the component
*/
const mapDispatchToProps = dispatch => {
    return {
        buyCake: () => dispatch(buyCake())
    };
};

/*
*
*/
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CakeContainer);