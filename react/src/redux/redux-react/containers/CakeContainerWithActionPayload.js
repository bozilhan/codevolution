import React, {useState} from 'react';
import {connect} from 'react-redux';
import {buyCake} from '../redux';
/*
* https://www.youtube.com/watch?v=B-jNaEx1Xfc&list=PLC3y8-rFHvwheJHvseC3I0HuYI2f46oAK&index=25
*/
const CakeContainerWithActionPayload = ({numOfCakes, buyCake}) => {
    const [number, setNumber] = useState(1);

    const onChangeInput = (e) => {
        setNumber(e.target.value);
    };

    return (
        <div>
            <h2>Number of cakes - {numOfCakes}</h2>
            <input
                type="text"
                value={number}
                onChange={onChangeInput}
            />
            <button onClick={() => buyCake(number)}>Buy {number} Cake</button>
        </div>
    );
};

const mapStateToProps = state => {
    return {
        numOfCakes: state.cake.numOfCakes
    };
};

const mapDispatchToProps = dispatch => {
    return {
        buyCake: number => dispatch(buyCake(number))
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CakeContainerWithActionPayload);