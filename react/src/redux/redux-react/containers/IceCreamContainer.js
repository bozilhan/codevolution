import React from 'react';
import {connect} from 'react-redux';
import {buyIceCream} from '../redux';

/*
* SCENARIO
* we start off(yola cikmak, koyulmak, baslamak) with our APPLICATION
* which is now a SIMPLE REACT application
*
* the state of the app is maintained SEPARATELY in the Redux Store
*
* our app is ALWAYS SUBSCRIBED to this redux store
* !!!HOWEVER the app CANNOT DIRECTLY UPDATE the STATE!!!
*
* if the app wants to update the state it has to DISPATCH(gondermek, yollamak,sevketmek) an action
* once(-ir -mez) an action has been dispatched the REDUCER than handles(islemek, ele almak) that action
* and UPDATES the CURRENT state
*
* as soon as(hemen, -ir -mez, -ince) the state is updated the value is than passed on to
* the app because the app is subscribed to store
*
* https://www.youtube.com/watch?v=g5l8xu6i4XQ&list=PLC3y8-rFHvwheJHvseC3I0HuYI2f46oAK&index=15
* FOLDER STRUCTURE
* codevolution/src/redux/redux-react/redux
* everything to do WITH redux will be contained in this PARTICULAR FOLDER
* next create folder by FEATURE for our cakeShop application we just have ONE FEATURE --> cakes
*/

/*
* CONNECT
* https://www.youtube.com/watch?v=gFZiQnM3Is4&list=PLC3y8-rFHvwheJHvseC3I0HuYI2f46oAK&index=18
* how do we get hold of the redux state and how do we dispatch an action from within
*
* 1. define a new function mapStateToProps
* 2. define a new function mapDispatchToProps
* 3. we are going to connect these two functions with our react component
*    for that we use the connect function import {connect} from 'react-redux';

* const IceCreamContainer = (props) => {
        <div>
            <h2>Number of cakes - {props.numOfIceCreams}</h2>
            <button onClick={props.buyIceCream}>Buy Cake</button>
        </div>
*/

/*
* numOfIceCreams from mapStateToProps
* buyIceCream from mapDispatchToProps
*
* HOW IT ALL WORKS
* first is mapStateToProps function
* when you want to access the redux state in your component you DEFINE mapStateToProps function
* it gets a redux state as a parameter which can be used to retrieve(geri almak) the APPROPRIATE STATE PROPERTIES
* in our case we map state.numOfIceCreams to a prop called numOfIceCreams which will then render in the JSX
*
* similarly for dispatching actions we have mapDispatchToProps function
* this function gets the dispatch method as a parameter and allows us
* to map ACTION CREATORS to props in our component
* in our example we map dispatching buyIceCream to a prop called buyIceCream
* this allows us to call buyIceCream as props.buyIceCream
*
* all this is possible BECAUSE OF the CONNECT FUNCTION from react-redux
* the CONNECT function connects a react component to the REDUX-STORE
* in our case CONNECT function connects IceCreamContainer to the REDUX-STORE
*
* in react-redux 7.1 hooks have been added so react-redux now offers
* a set of hook APIs as an ALTERNATIVE to existing CONNECT hoc
* the APIs allow you to subscribe to the redux store and dispatch actions
* WITHOUT having to wrap your components and connect
*/
const IceCreamContainer = ({numOfIceCreams, buyIceCream}) => {
    return (
        <div>
            <h2>Number of ice creams - {numOfIceCreams}</h2>
            <button onClick={buyIceCream}>Buy Ice Cream</button>
        </div>
    );
};

/*
* this function gets a redux state as a parameter and
* returns an object
*
* the state from the redux store is MAPPED to our component PROPS
* it REFLECTS numOfIceCreams in the redux store
*/
const mapStateToProps = state => {
    return {
        numOfIceCreams: state.iceCream.numOfIceCreams
    };
};

/*
* this function gets a redux dispatch as a parameter and
* returns an object
*
* map our dispatch of an action creator to a PROP in our component
* our component now receives a 2nd additional prop called buyIceCream
* which will basically dispatch the buy cake action
*/
const mapDispatchToProps = dispatch => {
    return {
        buyIceCream: () => dispatch(buyIceCream())
    };
};

/*
*
*/
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(IceCreamContainer);