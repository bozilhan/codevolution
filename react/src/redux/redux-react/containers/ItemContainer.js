/*
* https://www.youtube.com/watch?v=prg6YzRcEvE&list=PLC3y8-rFHvwheJHvseC3I0HuYI2f46oAK&index=26
* mapStateToProps
*
* https://www.youtube.com/watch?v=gdNitBetNWc&list=PLC3y8-rFHvwheJHvseC3I0HuYI2f46oAK&index=27
* mapDispatchToProps
*
* display either numOfCakes or numOfIceCreams
* BASED on a prop that is passed from the PARENT CONTAINER
*
* ItemContainer can be reused for both cakes and ice creams
*
* const ItemContainer = (props)
* <h2>Items - {props.item} </h2>
* <button onClick={props.buyItem}>Buy Items</button>
* da olur
*/
import React from 'react';
import {connect} from 'react-redux';
import {buyCake, buyIceCream} from '../redux';

const ItemContainer = ({item, buyItem}) => {
    return (
        <div>
            <h2>Items - {item} </h2>
            <button onClick={buyItem}>Buy Items</button>
        </div>
    );
};

/*
* this function takes two parameters
* the 1st parameter is redux state
* the 2nd parameter is the props of the component ITSELF
* which by CONVENTION is referred to as ownProps
*
* what this basically tries to convey(iletmek, tasimak, nakletmek, yaymak)
* is that hey I know that you're mapping state to component props
* BUT here are a few OWN props that the component has
*
* you can make use of it if you want to
*
* what we are going to do is CONDITIONALLY ASSIGNED the redux state
*
* we have used ownProps might be bit !!!RARE!!! in applications
*
* COMMON USE CASE is MASTER DETAILED PATTERN
* a list of items when you click on a particular item
* you would pass in the itemID as a prop
* and then fetch the data only for that ID from redux
*
*
* using second parameter COMPLETELY DEPENDS ON YOUR REQUIREMENTS
*/
const mapStateToProps = (state, ownProps) => {
    /*
    * we are going to pass in a prop called cake
    * from the PARENT COMPONENT
    */
    const itemState = ownProps.cake ?
        state.cake.numOfCakes : state.iceCream.numOfIceCreams;

    /*
    * return an object where the key is item
    */
    return {
        item: itemState
    };
};

/*
* CONDITIONALLY DISPATCH either by cake or by ice cream
* BASED on the prop that is passed from the PARENT COMPONENT
*/
const mapDispatchToProps = (dispatch, ownProps) => {
    const dispatchFunction = ownProps.cake ?
        () => dispatch(buyCake()) :
        () => dispatch(buyIceCream());

    return {
        buyItem: dispatchFunction
    };
};

export default connect(
    mapStateToProps, mapDispatchToProps)
(ItemContainer);

/*
* if you want to only dispatch actions
* but NOT SUBSCRIBE to the STATE CHANGES in the store
* pass null as the first argument to connect
*
* export default connect(
    null, mapDispatchToProps)
(ItemContainer);
*/