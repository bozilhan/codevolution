import React from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {buyCake} from '../redux';
/*
* useSelector
* useSelector is a hook the react-redux library provides
* which acts as a closed equivalent to mapStateToProps function
* so to get hold of any state that is maintained in the redux store
* we use the useSelector hook
*
* for our example we need to access theNumberOfCakes which is stored in the redux store
* 
*
* A potential obstacle with useSelector is that it uses strict equality
* which checked IF the fields changed. 
* This can cause potential problems when trying to return an object from useSelector, 
* so it's BEST PRACTICE to call useSelector once FOR EACH VALUE OF YOUR STATE.
*
*
* useDispatch
*
*/
const HooksCakeContainer = () => {
    /*
    * call useSelector
    * useSelector accepts a function as its parameter and this function is
    * called as the SELECTOR function
    * the SELECTOR FUNCTION receives the redux state as its argument
    * very similar to mapStateToProps function
    * the SELECTOR FUNCTION can than return a value
    * for our example we need to return state.numOfCakes
    */
    const numOfCakes = useSelector(
        state => state.cake.numOfCakes
    );

    /*
    * useDispatch hook returns a reference to dispatch function from the REDUX STORE
    *
    */
    const dispatch = useDispatch();

    return (
        <div>
            <h2>Num of cakes - {numOfCakes}</h2>
            <button onClick={() => dispatch(buyCake())}>Buy Cake</button>
        </div>
    );
};

export default HooksCakeContainer;