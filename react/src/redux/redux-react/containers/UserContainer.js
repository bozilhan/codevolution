/*
 * add thunk middleware to codevolution/src/redux/redux-react/redux/store.js
 *    what this allows is for an action creator to RETURN A FUNCTION instead of an action
 *    the fetchAsyncUsers function in codevolution/src/redux/redux-react/redux/fetchUsers/fetchUsersActions.js
 *    now can perform side effects such as fetching data
 *
 * fetchAsyncUsers function also can dispatch regular actions based on the response
 *   this actions will then be handled by the REDUCER which UPDATES the redux STATE
 *
 * when the STATE is UPDATED the component which has subscribed to the state changes
 *   will receive the updated state which can than be used in the JSX
 */

/*
* https://www.youtube.com/watch?v=tQ80uAyqVyI&list=PLC3y8-rFHvwheJHvseC3I0HuYI2f46oAK&index=28
* https://www.youtube.com/watch?v=tcCS4mGAq7Q&list=PLC3y8-rFHvwheJHvseC3I0HuYI2f46oAK&index=29
* Synchronous Actions
* as soon as an action is dispatched, the state is IMMEDIATELY updated
* if you dispatch BUY_CAKE action, the numOfCakes right away(hemen, derhal, aninda) decrements by 1
* if you dispatch BUY_ICECREAM action, the numOfIceams right away(hemen, derhal, aninda) decrements by 1
*
* Async Actions
* you WAIT FOR a task to COMPLETE before DISPATCHING your action
* Typical use case is making API call WAITING FOR the RESPONSE and
* then DISPATCHING an action BASED ON the response
* Asynchronous API calls to FETCH data from an end point and use that data in your app
*
* STATE
* Typically with data fetching we go with 3 properties for the state object
* 1. loading flag --> which indicates whether the data is currently being fetched or not
*    Display a LOADING SPINNER in the component
* 2. data itself --> the initial state is empty array []
*    LIST OF USERS
* 3. error message --> our API request might fail for some reason.
*    In that scenario INSTEAD OF getting back the data we get an error which we store the error property
*    DISPLAY ERROR TO THE USER
*
*
* ACTIONS
* we have 3 actions
* 1. FETCH_USERS_REQUEST --> to fetch the list of users from the API endpoint
* 2. this action is dependent on the FETCH_USERS_REQUEST action. The data is fetched SUCCESSFULLY
*    we have an action FETCH_USERS_SUCCESS
* 3. if there was an error fetching the data we have an action type as FETCH_USERS_FAILURE
*
*
* REDUCERS
*    switch (action.type) {
        case FETCH_USERS_REQUEST:
            return {
                loading: true
            };

        case FETCH_USERS_SUCCESS:
            return {
                loading: false,
                users: data (from API)
            };

        case FETCH_USERS_FAILURE:
            return {
                loading: false,
                error: error (from API)
            };
    }
*/

import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {fetchAsyncUsers, fetchUser} from '../redux';
/*
 * the component where we render the list of users fetched from API
 */
const UserContainer = ({usersData, userData, fetchUser, fetchUsers}) => {
    const [id, setId] = useState(1);
    /*
     * dispatch the action in useEffect
     */
    useEffect(() => {
        fetchUsers();
        /*
         * specify EMPTY DEPENDENCY ARRAY
         * so that fetchUsers is dispatched
         * !!!ONLY ONCE!!!
         */
    }, [fetchUsers]);

    useEffect(() => {
        fetchUser();
    }, [fetchUser]);

    const onChangeInput = e => {
        setId(e.target.value);
    };

    const onClickFetchPost = () => {
        // id value which is the text input field value
        fetchUser(id);
    };

    return (
        <div>
            <input
                type='text'
                value={id}
                onChange={onChangeInput}/>
            <button
                type='button'
                onClick={onClickFetchPost}>
                Fetch User {id}
            </button>
        </div>
    );
};

const mapStateToProps = state => {
    return {
        usersData: state.users,
        userData: state.user
    };
};

const mapDispatchToProps = dispatch => {
    return {
        fetchUsers: () => dispatch(fetchAsyncUsers()),
        fetchUser: id => dispatch(fetchUser(id))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(UserContainer);


//                {usersData.loading} ? (<h2>Loading</h2>) : {usersData.error} ? (
//                 <h2>{usersData.error}</h2>) : (
//                 <div>
//                     <h2>User List</h2>
//                     <div>
//                         {usersData &&
//                         usersData.users &&
//                         usersData.users.map(eachUser => <p id={eachUser.id}>{eachUser.name}</p>)}
//                     </div>
//                 </div>
//                 )

//     return userData.loading ?
//         (<h2>Loading</h2>) :
//         userData.error ? (<h2>userData.error</h2>) :
//             (<div>
//                 <h2>User List</h2>
//                 <div>
//                     {
//                         userData &&
//                         userData.users &&
//                         userData.users.map(eachUser => <p>{eachUser.name}</p>)
//                     }
//                 </div>
//             </div>);

//                 <p>UserData:{id} --> {userData}</p>