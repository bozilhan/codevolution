const redux = require('redux');
const createStore = redux.createStore;

// Multiple Reducers One Initial State And One Reducer
// https://www.youtube.com/watch?v=apcda524MJA&list=PLC3y8-rFHvwheJHvseC3I0HuYI2f46oAK&index=9

const BUY_CAKE = 'BUY_CAKE';
const BUY_ICE_CREAM = 'BUY_ICE_CREAM';

// action creator
// The action creator should just be RESPONSIBLE FOR SENDING AN ACTION.
const buyCake = () => {
return {
        type: BUY_CAKE,
        info: 'First redux action'
    };
};

const buyIceCream = () => {
    return {
        type: BUY_ICE_CREAM
    };
};

// REDUCER
// (stateOfTheApplicationBeforeMakingAnyChange, action) => newState
const initialState = {
    numOfCakes: 10,
    numOfIceCreams: 20
};

/*
* This approach of using just ONE REDUCER definitely works
* BUT that would be DIFFICULT TO DEBUG, MANAGE AND KEEP TRACK OFF
* */
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case BUY_CAKE:
            return {
                ...state,
                numOfCakes: state.numOfCakes - 1
            };

        case BUY_ICE_CREAM:
            return {
                ...state,
                numOfIceCreams: state.numOfIceCreams - 1
            };

        default:
            return state;
    }
};

// 1st responsibility
const store = createStore(reducer);

// 2nd responsibility
console.log('Initial state', store.getState());

// 4th responsibility
// subscribe method accepts a function
const unsubscribe = store.subscribe(
    () => {
        console.log('Updated state', store.getState());
    }
);

// 3rd responsibility
// dispatch method accepts an ACTION or ACTION CREATOR(should return an ACTION) as its parameter
// ALWAYS USE ACTIONS CREATOR INSTEAD OF ACTION!!!
// to cause a few more state transitions we dispatch the same action two more times
// The action creator should just be RESPONSIBLE FOR SENDING AN ACTION.
store.dispatch(buyCake());
store.dispatch(buyCake());
store.dispatch(buyCake());
store.dispatch(buyIceCream());
store.dispatch(buyIceCream());

// 5th responsibility
unsubscribe();