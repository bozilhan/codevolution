const redux = require('redux');
const reduxLogger = require('redux-logger');

const createStore = redux.createStore;
const combineReducers = redux.combineReducers;

// https://www.youtube.com/watch?v=8zPyXAWS0L4&list=PLC3y8-rFHvwheJHvseC3I0HuYI2f46oAK&index=11
const applyMiddleware = redux.applyMiddleware;
const logger = reduxLogger.createLogger();

// MultipleReducerSplitInitialStatesAndReducers
// https://www.youtube.com/watch?v=apcda524MJA&list=PLC3y8-rFHvwheJHvseC3I0HuYI2f46oAK&index=9
// https://www.youtube.com/watch?v=apcda524MJA&list=PLC3y8-rFHvwheJHvseC3I0HuYI2f46oAK&index=10

const BUY_CAKE = 'BUY_CAKE';
const BUY_ICE_CREAM = 'BUY_ICE_CREAM';

// action creators
// The action creator should just be RESPONSIBLE FOR SENDING AN ACTION.
const buyCake = () => {
    return {
        type: BUY_CAKE,
        info: 'First redux action'
    };
};

const buyIceCream = () => {
    return {
        type: BUY_ICE_CREAM
    };
};

// MULTIPLE REDUCERS
// SPLIT OUR INITIAL STATE INTO TWO
const initialCakeState = {
    numOfCakes: 10
};

const initialIceCreamState = {
    numOfIceCreams: 20
};

/* MULTIPLE REDUCERS
 SPLIT OUR REDUCER STATE INTO TWO
*/

/* cakeReducer is ONLY interested in cakeState and
 the logic to update the cakeState
 it has nothing to do with ICE CREAM
 */
const cakeReducer = (state = initialCakeState, action) => {
    switch (action.type) {
        case BUY_CAKE:
            return {
                ...state,
                numOfCakes: state.numOfCakes - 1
            };

        default:
            return state;
    }
};

/* iceCreamReducer is ONLY interested in iceCreamState and
 the logic to update the iceCreamState
 it has nothing to do with CAKE
 */
const iceCreamReducer = (state = initialIceCreamState, action) => {
    switch (action.type) {
        case BUY_ICE_CREAM:
            return {
                ...state,
                numOfIceCreams: state.numOfIceCreams - 1
            };

        default:
            return state;
    }
};

// 1st responsibility

/*
* COMBINE MULTIPLE REDUCERS
*
* redux provides a method called combined reducers
* to COMBINE MULTIPLE REDUCERS into SINGLE which can then be passed
* to createStore() method
*
* before we create store we COMBINE our REDUCERS
*
* combineReducers method accepts an object
* each key, value pair for this object CORRESPONDS to a REDUCER
* */

const rootReducer = combineReducers({
    cake: cakeReducer,
    iceCream: iceCreamReducer
});

const store = createStore(rootReducer, applyMiddleware(logger));

// 2nd responsibility
console.log('Initial state', store.getState());

// 4th responsibility
// subscribe method accepts a function
const unsubscribe = store.subscribe(
    () => {
    }
);

// 3rd responsibility
// dispatch method accepts an ACTION or ACTION CREATOR(should return an ACTION) as its parameter
// ALWAYS USE ACTIONS CREATOR INSTEAD OF ACTION!!!
// to cause a few more state transitions we dispatch the same action two more times
store.dispatch(buyCake());
store.dispatch(buyCake());
store.dispatch(buyCake());
store.dispatch(buyIceCream());
store.dispatch(buyIceCream());

// 5th responsibility
unsubscribe();