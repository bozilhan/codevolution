const redux = require('redux');
const createStore = redux.createStore;

/*
 * when your app GROWS in size you can split the reducers
 * into separate files and keep them completely independent
 * and managing different areas
 * */

/*
 * CALISTIRMA
 * Ilgili dizinde terminal ac
 * node indexOneReducer
 *
 *
 * Create ACTION for Cake Shop Application
 *
 * First we define a string constant that indicates the TYPE OF THE ACTION
 * AVOID SPELLING MISTAKES when USING action
 *
 * Second DEFINE AN ACTION.
 * Action is object that has a 'type' and 'payload' property
 *
 * other than 'type' the structure of an action object
 * is completely up to you
 * `So you can have another property that is an object or
 * a really simple property that is just a string
 *
 * ACTION CREATOR is a function that RETURNS an ACTION OBJECT
 * ACTIONS describe WHAT HAPPENED
 * but ACTIONS DONT DESCRIBE HOW the applications' STATE CHANGES
 *
 *
 * REDUCERS specify how app's state changes in response to actions sent to the store
 * REDUCERS is responsible for HOW the applications' STATE CHANGES
 * REDUCERS are functions that ACCEPTS STATE and ACTION as argument and
 * RETURNS THE NEXT STATE of the application
 * (stateOfTheApplicationBEFOREMakingANYChange, action) => newState
 *
 * reducer action ve statei bilir. Reducer da kullanilmak istenen veri ACTION nesnesine
 * PROPERTY olarak eklenir.
 * Action creator DISARIYA ACILAN YUZUMUZ.
 * Action Creatoru cagiran yerden alinacak veri Action Creatore parametre olarak verilir.
 * Ilgili parametreler plain JS Object propertyleri(key,value pair) olarak tutulur ve
 * REDUCDER a AKTARILIR.
 * REDUCER metodlarina state ve action dan BASKA PARAMETRE EKLENMEZ.
 * TUM VERI ACTIONdan ALINIR!!!
 * REDUCER ACTIONi BILIR.
 * REDUCER a ne gerekiyorsa ACTION UZERINDEN GERCEKLESTIRILIR.
 * REDUCER DIS DUNYADAN VERI ALMAZ DIS DUNYAYI BILMEZ
 *
 *
 * YOU NEVER MUTATE THE STATE OBJECT IN REDUCER!!!! return NEW OBJECT
 *
 * Your state object might contain MORE THAN ONE PROPERTY
 * that is why always SAFER TO FIRST COPY OF THE STATE OBJECT
 * and then CHANGE ONLY THE PROPERTIES THAT NEED TO
 *
 * To make the copy of the state object we use SPREAD OPERATOR
 *
 * STORE
 * ONE store for entire application. TUM UYGULAMADA 1 TANE STORE INSTANCE OLMALI!!!
 * SINGLETON
 * store is responsible for HOLDING APPLICATION STATE
 * store has to have getState() method which gives our application access to the state it holds
 * store provides a method called DISPATCH to allow(izin/imkan vermek) to the application state
 * DISPATCH method accepts an ACTION as its parameter
 * store also allows our application to REGISTER LISTENERS through the SUBSCRIBE method
 * SUBSCRIBE method accepts a function as its parameter which is EXECUTED ANY TIME
 * the state in the redux store CHANGES
 * you can also unsubscribe to the store by calling the function that was
 * returned by the SUBSCRIBE method
 *
 * our goal is to implement all these responsibilities
 */

const BUY_CAKE = 'BUY_CAKE';

// action creator
const buyCake = () => {
  return {
    type: BUY_CAKE,
    info: 'First redux action',
  };
};

// REDUCER
// (stateOfTheApplicationBeforeMakingAnyChange, action) => newState
const initialState = {
  numOfCakes: 10,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case BUY_CAKE:
      return {
        /*
         * NEVER MUTATE THE STATE OBJECT RETURN NEW OBJECT
         *
         * Your state object might contain MORE THAN ONE PROPERTY
         * that is why always SAFER TO FIRST COPY OF THE STATE OBJECT
         * and then CHANGE ONLY THE PROPERTIES THAT NEED TO
         *
         * To make the copy of the state object we use SPREAD OPERATOR
         *
         * Asking the reducer to first make a copy of the state object
         * and then ONLY UPDATE numOfCakes
         * if there were other properties they would REMAIN UNCHANGED!!!
         *
         * CURRENT NUMBER OF CAKES -1
         */
        ...state,
        numOfCakes: state.numOfCakes - 1,
      };

    default:
      return state;
  }
};

/*
 * https://www.youtube.com/watch?v=YAevAj6X6XU&list=PLC3y8-rFHvwheJHvseC3I0HuYI2f46oAK&index=7
 * 07:00/10:32 aciklama onemli
 * STORE
 * 1. store has to hold the application state
 * implement edilecek store initialState nesnesini tutmali
 * redux library provide method called create store
 * createStore method accepts a parameter which is the REDUCER FUNCTION
 *
 * 2. expose a method called get state which gives the current state in the store
 *
 * 3. store provides DISPATCH method to UPDATE the STATE
 *
 * 4. allow the app to subscribe to changes in the store that is achieved
 * using SUBSCRIBE method
 *
 * 5. unsubscribe from store by calling the function
 * returned by the subscribe method
 * AFTER all our code HAS COMPLETED we can call the UNSUBSCRIBE method
 *
 * SUMMARY ESSENCE(esas/oz/ruh) OF REDUX
 * 1. Create a Store
 * 2. declare the initial state and the reducer
 * 3. define your actions and action creators
 * 4. subscribe to the store
 * 5. dispatch actions to update the store
 * 6. subscribe to the changes
 *
 * when we dive into react application this is exactly what we are going to do
 * but with a few helper functions
 */

// 1st responsibility
const store = createStore(reducer);

// 2nd responsibility
console.log('Initial state', store.getState());

// 4th responsibility
// subscribe method accepts a function
const unsubscribe = store.subscribe(() => {
  console.log('Updated state', store.getState());
});

// 3rd responsibility
// dispatch method accepts an ACTION or ACTION CREATOR(should return an ACTION) as its parameter
// ALWAYS USE ACTIONS CREATOR INSTEAD OF ACTION!!!
// to cause a few more state transitions we dispatch the same action two more times
store.dispatch(buyCake());
store.dispatch(buyCake());
store.dispatch(buyCake());

// 5th responsibility
unsubscribe();
