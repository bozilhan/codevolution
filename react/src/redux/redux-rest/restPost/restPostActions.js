import {FAILURE_POST_, REQUEST_POST_, SUCCESS_POST_} from './restPostTypes';

export const postRequest = (entityName) => {
  return {
    type: `${REQUEST_POST_}${entityName.toUpperCase()}`,
    entityName: entityName,
  };
};

export const postSuccess = (entityName, postedData) => {
  // console.log('\n\n\n\n POST postedData:', postedData);
  return {
    type: `${SUCCESS_POST_}${entityName.toUpperCase()}`,
    payload: postedData,
    entityName: entityName,
  };
};

export const postFailure = (entityName, error) => {
  // console.log('\n\n\n\n POST error:', error);
  return {
    type: `${FAILURE_POST_}${entityName.toUpperCase()}`,
    payload: error,
    entityName: entityName,
  };
};

export const postEntity = (entityName, data) => (dispatch) => {
  dispatch(postRequest(entityName));

  return (
    fetch(`https://bozilhan-platform.firebaseio.com/${entityName}.json`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    })
      // then
      // USTEKILERIN HEPSI TAMAMLANDIKTAN SONRA
      // BURAYA DEVAM ET VE BURADAKILERI DE  GERCEKLESTIR.
      .then((response) => response.json())
      .then((responseData) => {
        dispatch(postSuccess(entityName, data));
      })
      .catch((error) => {
        dispatch(postFailure(entityName, error));
      })
  );
};
