/*
 * we combine all our reducers
 * and export ONE SINGLE REDUCER
 * that can be passed to the createStore function
 */
import {combineReducers} from 'redux';
import fetchReducer from './genericRest/restFetch/restFetchReducer';
import postReducer from './genericRest/restPost/restPostReducer';
import putReducer from './genericRest/restPut/restPutReducer';
import {
    enAracBilgileri,
    enBaslangicYeri,
    enBelgeTipi,
    enBitisYeri,
    enBogazGecisi,
    enCinsiyet,
    enFaaliyetBilgileri,
    enFaaliyetTuru,
    enGemiBilgileri,
    enGemiRengi,
    enGemiTutumu,
    enIlgiliGemiDenizAraci,
    enIlgiliTesis,
    enKayitliOlduguLiman,
    enKGK,
    enKisiBilgileri,
    enLastikBotDurumu,
    enMuharebeCihazBilgileri,
    enTesisBilgileri,
    enTipiGorevi,
    enTipiHava,
    enTipiKara,
    enTuru,
    enUyruk,
    enYasAraligi
} from './entityNames';


const createNamedWrapperReducer = (reducerFunction, reducerName) => {
    return (state, action) => {
        const {entityName} = action;
        const isInitializationCall = state === undefined;
        if (entityName !== reducerName && !isInitializationCall) {
            return state;
        }
        return reducerFunction(state, action);
    };
};

const rootReducer = combineReducers({
    platformFetchKGK: createNamedWrapperReducer(fetchReducer, enKGK),
    platformFetchGemiRengi: createNamedWrapperReducer(fetchReducer,
    enGemiRengi),
    platformFetchKayitliOlduguLiman: createNamedWrapperReducer(
    fetchReducer,
    enKayitliOlduguLiman
    ),
    platformFetchBogazGecisi: createNamedWrapperReducer(fetchReducer,
    enBogazGecisi),
    platformFetchGemiTutumu: createNamedWrapperReducer(fetchReducer,
    enGemiTutumu),
    platformFetchFaaliyetBilgileri: createNamedWrapperReducer(
    fetchReducer,
    enFaaliyetBilgileri
    ),
    platformFetchMuharebeCihazBilgileri: createNamedWrapperReducer(
    fetchReducer,
    enMuharebeCihazBilgileri
    ),
    platformFetchGemiBilgileri: createNamedWrapperReducer(
    fetchReducer,
    enGemiBilgileri
    ),
    platformFetchFaaliyetTuru: createNamedWrapperReducer(
    fetchReducer,
    enFaaliyetTuru
    ),
    platformFetchBaslangicYeri: createNamedWrapperReducer(
    fetchReducer,
    enBaslangicYeri
    ),
    platformFetchBitisYeri: createNamedWrapperReducer(
    fetchReducer,
    enBitisYeri
    ),
    platformFetchLastikBotDurumu: createNamedWrapperReducer(
    fetchReducer,
    enLastikBotDurumu
    ),
    platformFetchIlgiliGemiDenizAraci: createNamedWrapperReducer(
    fetchReducer,
    enIlgiliGemiDenizAraci
    ),
    platformFetchIlgiliTesis: createNamedWrapperReducer(
    fetchReducer,
    enIlgiliTesis
    ),
    platformFetchCinsiyet: createNamedWrapperReducer(
    fetchReducer,
    enCinsiyet
    ),
    platformFetchUyruk: createNamedWrapperReducer(
    fetchReducer,
    enUyruk
    ),
    platformFetchTipiGorevi: createNamedWrapperReducer(
    fetchReducer,
    enTipiGorevi
    ),
    platformFetchYasAraligi: createNamedWrapperReducer(
    fetchReducer,
    enYasAraligi
    ),
    platformFetchBelgeTipi: createNamedWrapperReducer(
    fetchReducer,
    enBelgeTipi
    ),
    platformFetchTuru: createNamedWrapperReducer(
    fetchReducer,
    enTuru
    ),
    platformFetchTipiKara: createNamedWrapperReducer(
    fetchReducer,
    enTipiKara
    ),
    platformFetchTipiHava: createNamedWrapperReducer(
    fetchReducer,
    enTipiHava
    ),
    platformFetchKisiBilgileri: createNamedWrapperReducer(
    fetchReducer,
    enKisiBilgileri
    ),
    platformFetchAracBilgileri: createNamedWrapperReducer(
    fetchReducer,
    enAracBilgileri
    ),
    platformFetchTesisBilgileri: createNamedWrapperReducer(
    fetchReducer,
    enTesisBilgileri
    ),
    platformPost: postReducer,
    platformPut: putReducer,
});

export default rootReducer;
