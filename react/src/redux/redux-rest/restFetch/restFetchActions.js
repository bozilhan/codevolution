import {FAILURE_FETCH_, REQUEST_FETCH_, SUCCESS_FETCH_} from './restFetchTypes';

export const fetchRequest = (entityName) => {
  return {
    type: `${REQUEST_FETCH_}${entityName.toUpperCase()}`,
    entityName: entityName,
  };
};

export const fetchSuccess = (entityName, fetchedData) => {
  // console.log('\n\n\n\n FETCH fetchedData:', fetchedData);
  return {
    type: `${SUCCESS_FETCH_}${entityName.toUpperCase()}`,
    payload: fetchedData,
    entityName: entityName,
  };
};

export const fetchFailure = (entityName, error) => {
  // console.log('\n\n\n\n FETCH error:', error);
  return {
    type: `${FAILURE_FETCH_}${entityName.toUpperCase()}`,
    payload: error,
    entityName: entityName,
  };
};

export const fetchEntity = (entityName) => (dispatch) => {
  dispatch(fetchRequest(entityName));

  return (
    fetch(`https://bozilhan-platform.firebaseio.com/${entityName}.json`)
      // then
      // USTEKILERIN HEPSI TAMAMLANDIKTAN SONRA
      // BURAYA DEVAM ET VE BURADAKILERI DE GERCEKLESTIR.
      .then((response) => response.json())
      .then((responseData) => {
        dispatch(fetchSuccess(entityName, responseData));
      })
      .catch((error) => {
        dispatch(fetchFailure(entityName, error));
      })
  );
};
