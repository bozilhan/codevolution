import {useCallback, useEffect, useMemo, useRef, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {fetchEntity} from '../redux/index';
import {
    enAracBilgileri,
    enBaslangicYeri,
    enBelgeTipi,
    enBitisYeri,
    enBogazGecisi,
    enCinsiyet,
    enFaaliyetBilgileri,
    enFaaliyetTuru,
    enGemiBilgileri,
    enGemiRengi,
    enGemiTutumu,
    enIlgiliGemiDenizAraci,
    enIlgiliTesis,
    enKayitliOlduguLiman,
    enKGK,
    enKisiBilgileri,
    enLastikBotDurumu,
    enMuharebeCihazBilgileri,
    enTesisBilgileri,
    enTipiGorevi,
    enTipiHava,
    enTipiKara,
    enTuru,
    enUyruk,
    enYasAraligi
} from '../redux/entityNames';


const entitySelectorFunction = {
    [enKGK]: state => state.platformFetchKGK.fetchedData,
    [enGemiRengi]: state => state.platformFetchGemiRengi.fetchedData,
    [enKayitliOlduguLiman]: state =>
    state.platformFetchKayitliOlduguLiman.fetchedData,
    [enBogazGecisi]: state => state.platformFetchBogazGecisi.fetchedData,
    [enGemiTutumu]: state => state.platformFetchGemiTutumu.fetchedData,
    [enFaaliyetBilgileri]: state =>
    state.platformFetchFaaliyetBilgileri.fetchedData,
    [enMuharebeCihazBilgileri]: state =>
    state.platformFetchMuharebeCihazBilgileri.fetchedData,
    [enGemiBilgileri]: state =>
    state.platformFetchGemiBilgileri.fetchedData,
    [enFaaliyetTuru]: state =>
    state.platformFetchFaaliyetTuru.fetchedData,
    [enBaslangicYeri]: state =>
    state.platformFetchBaslangicYeri.fetchedData,
    [enBitisYeri]: state =>
    state.platformFetchBitisYeri.fetchedData,
    [enLastikBotDurumu]: state =>
    state.platformFetchLastikBotDurumu.fetchedData,
    [enIlgiliGemiDenizAraci]: state =>
    state.platformFetchIlgiliGemiDenizAraci.fetchedData,
    [enIlgiliTesis]: state =>
    state.platformFetchIlgiliTesis.fetchedData,
    [enCinsiyet]: state =>
    state.platformFetchCinsiyet.fetchedData,
    [enUyruk]: state =>
    state.platformFetchUyruk.fetchedData,
    [enTipiGorevi]: state =>
    state.platformFetchTipiGorevi.fetchedData,
    [enYasAraligi]: state =>
    state.platformFetchYasAraligi.fetchedData,
    [enBelgeTipi]: state =>
    state.platformFetchBelgeTipi.fetchedData,
    [enTuru]: state =>
    state.platformFetchTuru.fetchedData,
    [enTipiHava]: state =>
    state.platformFetchTipiHava.fetchedData,
    [enTipiKara]: state =>
    state.platformFetchTipiKara.fetchedData,
    [enKisiBilgileri]: state =>
    state.platformFetchKisiBilgileri.fetchedData,
    [enAracBilgileri]: state =>
    state.platformFetchAracBilgileri.fetchedData,
    [enTesisBilgileri]: state =>
    state.platformFetchTesisBilgileri.fetchedData
};

const initialData = [];

export const useFetch = entityName => {
    const dispatch = useDispatch();
    const [data, setData] = useState(initialData);
    const desiredData = useSelector(entitySelectorFunction[entityName]);
    const componentJustMounted = useRef(true);
    
    useMemo(() => {
        if (!componentJustMounted.current) {
            setData(desiredData);
        }
        componentJustMounted.current = false;
    }, [desiredData]);
    
    const fetchedCB = useCallback(() => {
        dispatch(fetchEntity(entityName));
    }, [dispatch, entityName]);
    
    useEffect(() => {
        fetchedCB();
    }, [fetchedCB]);
    
    return data;
};