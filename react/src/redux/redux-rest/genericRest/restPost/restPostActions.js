import {FAILURE_POST_, REQUEST_POST_, SUCCESS_POST_} from './restPostTypes';


export const postRequest = entityName => {
  return {
    type: `${REQUEST_POST_}${entityName.toUpperCase()}`,
    entityName: entityName
  };
};

export const postSuccess = (entityName, postedData) => {
  return {
    type: `${SUCCESS_POST_}${entityName.toUpperCase()}`,
    payload: postedData,
    entityName: entityName
  };
};

export const postFailure = (entityName, error) => {
  return {
    type: `${FAILURE_POST_}${entityName.toUpperCase()}`,
    payload: error,
    entityName: entityName
  };
};

export const postEntity = (entityName, data) => async dispatch => {
  dispatch(postRequest(entityName));
  
  const response = await fetch(
  `https://bozilhan-platform.firebaseio.com/${entityName}.json`,
  {
    method: 'POST',
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json'
    }
  }
  ).catch(error => dispatch(postFailure(entityName, error)));
  
  if (!response) {
    return;
  }
  
  dispatch(postSuccess(entityName, data));
};
