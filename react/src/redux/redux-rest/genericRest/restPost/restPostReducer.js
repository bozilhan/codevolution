import {FAILURE_POST_, REQUEST_POST_, SUCCESS_POST_} from './restPostTypes';


const initialState = {
    loading: false,
    postedData: {},
    error: '',
    entityName: ''
};

const postReducer = (state = initialState, action) => {
    if (!action || !action.entityName) {
        return state;
    }
    
    switch (action.type) {
        case REQUEST_POST_ + action.entityName.toUpperCase():
            return {
                ...state,
                loading: true,
                entityName: action.entityName
            };
        case SUCCESS_POST_ + action.entityName.toUpperCase():
            return {
                loading: false,
                postedData: action.payload,
                error: '',
                entityName: action.entityName
            };
        case FAILURE_POST_ + action.entityName.toUpperCase():
            return {
                loading: false,
                postedData: {},
                error: action.payload,
                entityName: action.entityName
            };
        default:
            return state;
    }
};

export default postReducer;