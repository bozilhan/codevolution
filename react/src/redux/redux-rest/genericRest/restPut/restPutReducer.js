import {FAILURE_PUT_, REQUEST_PUT_, SUCCESS_PUT_} from './restPutTypes';


const initialState = {
    loading: false,
    postedData: {},
    error: ''
};

const putReducer = (state = initialState, action) => {
    if (!action || !action.entityName) {
        return state;
    }
    
    switch (action.type) {
        case REQUEST_PUT_ + action.entityName.toUpperCase():
            return {
                ...state,
                loading: true
            };
        case SUCCESS_PUT_ + action.entityName.toUpperCase():
            return {
                loading: false,
                putData: action.payload,
                error: ''
            };
        case FAILURE_PUT_ + action.entityName.toUpperCase():
            return {
                loading: false,
                putData: {},
                error: action.payload
            };
        default:
            return state;
    }
};

export default putReducer;