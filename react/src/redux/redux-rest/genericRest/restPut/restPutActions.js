import {FAILURE_PUT_, REQUEST_PUT_, SUCCESS_PUT_} from './restPutTypes';


export const putRequest = entityName => {
    return {
        type: `${REQUEST_PUT_}${entityName.toUpperCase()}`,
    };
};

export const putSuccess = (entityName, putData) => {
    return {
        type: `${SUCCESS_PUT_}${entityName.toUpperCase()}`,
        payload: putData
    };
};

export const putFailure = (entityName, error) => {
    return {
        type: `${FAILURE_PUT_}${entityName.toUpperCase()}`,
        payload: error
    };
};

export const putEntity = (entityName, id, data) => async dispatch => {
    dispatch(putRequest(entityName));
    
    const response = await fetch(
    `https://bozilhan-platform.firebaseio.com/${entityName}/${id}.json`,
    {
        method: 'PUT',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    }
    ).catch(error => dispatch(putFailure(entityName, error)));
    
    if (!response) {
        return;
    }
    
    dispatch(putSuccess(entityName, data));
};
