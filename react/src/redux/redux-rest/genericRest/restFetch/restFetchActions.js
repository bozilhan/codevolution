import {FAILURE_FETCH_, REQUEST_FETCH_, SUCCESS_FETCH_} from './restFetchTypes';


export const fetchRequest = entityName => {
  return {
    type: `${REQUEST_FETCH_}${entityName.toUpperCase()}`,
    entityName: entityName
  };
};

export const fetchSuccess = (entityName, fetchedData) => {
  return {
    type: `${SUCCESS_FETCH_}${entityName.toUpperCase()}`,
    payload: fetchedData,
    entityName: entityName
  };
};

export const fetchFailure = (entityName, error) => {
  return {
    type: `${FAILURE_FETCH_}${entityName.toUpperCase()}`,
    payload: error,
    entityName: entityName
  };
};

export const fetchEntity = entityName => async dispatch => {
  dispatch(fetchRequest(entityName));
  
  const response = await fetch(
  `https://bozilhan-platform.firebaseio.com/${entityName}.json`
  ).catch(error => dispatch(fetchFailure(entityName, error)));
  
  if (!response) {
    return;
  }
  
  const responseData = await response
  .json()
  .catch(error => dispatch(fetchFailure(entityName, error)));
  
  if (!responseData) {
    return;
  }
  
  dispatch(fetchSuccess(entityName, responseData));
};
