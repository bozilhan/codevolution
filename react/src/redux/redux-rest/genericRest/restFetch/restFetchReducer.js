import {FAILURE_FETCH_, REQUEST_FETCH_, SUCCESS_FETCH_} from './restFetchTypes';


const initialState = {
    loading: false,
    fetchedData: [],
    error: '',
    entityName: ''
};

const fetchReducer = (state = initialState, action) => {
    if (!action || !action.entityName) {
        return state;
    }
    
    switch (action.type) {
        case REQUEST_FETCH_ + action.entityName.toUpperCase():
            return {
                ...state,
                loading: true,
                entityName: action.entityName
            };
        case SUCCESS_FETCH_ + action.entityName.toUpperCase():
            return {
                loading: false,
                fetchedData: action.payload,
                error: '',
                entityName: action.entityName
            };
        case FAILURE_FETCH_ + action.entityName.toUpperCase():
            return {
                loading: false,
                fetchedData: [],
                error: action.payload,
                entityName: action.entityName
            };
        default:
            return state;
    }
};

export default fetchReducer;