import React from 'react';
/*
* to provide the redux store to our react app
* the react-redux library exports a component called Provider
*
* WHERE you provide the store typically it is at the top of the
* App Component because doing so will provide the store to basically
* every component in your application component tree
*
* USAGE WARNINGS!!!
*/
import {Provider} from 'react-redux';

/*
* our store passed to Provider component as prop
*/
import store from './redux-react/redux/store';
import UserContainer from './redux-react/containers/UserContainer';

const ReduxApp = () => {
    return (
        <Provider store={store}>
            <div>
                <UserContainer/>
                {/* <ItemContainer cake/>
                <ItemContainer/>
                <HooksCakeContainer/>
                <CakeContainer/>
                <IceCreamContainer/>
                <CakeContainerWithActionPayload/> */}
            </div>
        </Provider>
    );
};

export default ReduxApp;