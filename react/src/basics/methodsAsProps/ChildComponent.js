import React from 'react';

/*
   passing a parameter when calling the parent method from the child component
   this is where an arrow function in the return statement is really useful

   arrow function syntax is the SIMPLEST WAY TO PASS PARAMETERS
   FROM THE CHILD COMPONENT TO PARENT COMPONENT

   onClick={() => props.greetHandler()}
   we can pass in any number of parameters to the greetHandleMethod
 */
const ChildComponent = (props) => {
  return (
    <div>
      <button onClick={() => props.greetHandler('child')}>Greet Parent</button>
    </div>
  );
};

export default ChildComponent;
