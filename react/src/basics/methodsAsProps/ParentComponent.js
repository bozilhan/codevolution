/*
  child component wants to COMMUNICATE with the PARENT COMPONENT
  we still use props but this time we PASS IN A REFERENCE TO
  A METHOD AS PROPS TO THE CHILD COMPONENT

  what i want is when we click on the button in the CHILD COMPONENT
  i want to execute method defined in the PARENT COMPONENT

  basically child component calls a parent component method
  this is achieved using props

  the only difference is we pass the METHOD ITSELF AS A PROP TO THE CHILD COMPONENT

  <ChildComponent greetHandler={this.greetParent} />
  we are passing to the reference to greetParent() method as a prop
  called greetHandler

  if parent DOESNT RE-RENDER the children ALSO WILL NEVER RE-RENDER
*/
import React, {Component} from 'react';
import ChildComponent from './ChildComponent';

class ParentComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      parentName: 'Parent'
    };

    this.greetParent = this.greetParent.bind(this);
  }

  /*
      since we use this keyword in the method
      we NEED TO BIND
    */
  greetParent(childName) {
    alert(`Hello ${this.state.parentName} from ${childName}`);
  }

  render() {
    return (
      <div>
        <ChildComponent greetHandler={this.greetParent} />
      </div>
    );
  }
}

export default ParentComponent;
