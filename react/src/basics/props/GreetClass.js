import React, {Component} from 'react';

/*
class components are basically ES6 classes.
Similar to a functional components class components also
can optionally receive props as input and return HTML

apart from the props a class component can also maintain a PRIVATE INTERNAL state.
It can maintain some information which is PRIVATE to THAT COMPONENT and USE that information to describe the UI

for this class to become react component
1. it should extend the component from react
2. the class has to implement render() method
  which will return NULL or SOME HTML
  render() method reads props and state and return JSX
  render method is pure function. for the given props and state it should always render the same UI
    what you SHOULD NOT HERE is
    changing the state of the component
    interacting with DOM
    making any AJAX calls

unlike the functional component where we specify the props parameter
in a class component the properties are available through
THIS.PROPS which is reserved in class components

props are IMMUTABLE, their value cannot be CHANGED!!!

ANYTHING within the components OPENING and CLOSING TAGS will be PASSED AS THE CHILDREN PROP
which is then accessed to render the UI
*/
class GreetClass extends Component {
  render() {
    /*
      in class components we GENERALLY tend to
      destructure or state IN the render() method
    */
    const {name, heroName} = this.props;

    return (
      <h1>
        Hello {name} a.k.a {heroName}Class Component
      </h1>
    );
  }
}

export default GreetClass;
