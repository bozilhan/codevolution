import React from 'react';

/*
5. Functional Components
functional components are just javascript functions.
they can OPTIONALLY receive an object of properties
which is REFERRED TO AS PROPS and return HTML which describes the UI.
HTML is actually KNOWN as JSX
*/

// function Greet() {
//   return <h1>Hello Wishwas</h1>;
// }

// const Greet = () => {
//   return <h1>Hello Wishwas</h1>;
// };

/*
NAMED export
you HAVE TO IMPORT component
with THE EXACT SAME NAME
App.js ye MyGreet olarak EKLEYEMEYIZ
./src/App.js
Attempted import error: './basics/components/Greet' does not contain a default export (imported as 'MyGreet').

CURLY BRACE le IMPORT EDILIR
import {Greet} from './basics/components/Greet'
*/

/*
8. props
retrieve(geri almak, getirmek) information/props from parent component
1. add parameter to functional component.
    you are going to call it props
    you can actually name this anything you want to but the convention is to name it props
2. use props parameter in the function body

props are IMMUTABLE, their value cannot be CHANGED!!!

ANYTHING within the components OPENING and CLOSING TAGS will be PASSED AS THE CHILDREN PROP
which is then accessed to render the UI

export const Greet = ({name, heroName}) => {
*/
export const Greet = (props) => {
  const {children, name, heroName} = props;

  /*
    yeri onemli degil
    const {name, heroName,children} = props;
  */

  return (
    <div>
      <h1>
        Hello {name} a.k.a {heroName} Functional Component
      </h1>
      {children}
    </div>
  );
};

/*
default exporting allows us TO IMPORT
the component with ANY NAME
in app.js we import as MyGreet

CURLY BRACE le OLMADAN import edilir
import Greet from './basics/components/Greet';

export default Greet;
*/
