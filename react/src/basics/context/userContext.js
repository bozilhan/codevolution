/*
Context provides a way to pass data through the component tree
WITHOUT having to pass props DOWN MANUALLY AT EVERY LEVEL

WHY context is NEEDED
TO AVOID passing props THROUGH EVERY LEVEL of component tree.
Context API will solve the problem

Our goal is to pass username value from App component and READ that value in F component
using context

There are 3 steps to implement when making use of context
1. Create the context

2. AT THE TOP LEVEL include Provide component and provide a value using 'value' attribute. this value can now be consumed in any of the descendant components
the PLACE you provide is IMPORTANT because only the DESCENDING COMPONENTS can CONSUME it
App component is usually a good place because all components fall under it
Provider Component is RESPONSIBLE FOR providing a VALUE for ALL DESCENDANT COMPONENTS
the value we want to provide is username
use 'value' attribute of Provider component
      <UserProvider value='Vishwas'>
        <ComponentC />
      </UserProvider>


3. Consume the context value in the NECESSARY/DESIRED COMPONENTS
for our demo we need to consume the username value in ComponentF
to consume context value we need to use consumer react component
in ComponentF in the render method as part of the return statement include UserConsumer component
in between the opening and closing tags of the consumer component we NEED TO SEPECIFY AN ARROW FUNCTION
the function gets the UserContext value AS ITS PARAMETER which can then be used within the function body TO RETURN react element
*/

/*
1. Create the context
we will use Create context method from react TO CREATE USER CONTEXT

EVERY context object created using React.createContext() method comes with
a PROVIDER and a CONSUMER react component
*/
import React from 'react';

/*
  default value will only be used 
  when the component DOES NOT HAVE a MATCHING PROVIDER
*/
const UserContext = React.createContext('codevolution');
const UserProvider = UserContext.Provider;
const UserConsumer = UserContext.Consumer;

/*
make sure to export UserProvider and UserConsumer in order to use for step 2,3
as well
*/
export {UserProvider, UserConsumer};

/*
  context type property
  there is another way to consume context value
  using context type property on a class
  1. export userContext itself
  2. assign this user context to the context type property on the class

  there are 2 limitations
  1. it only works for class components
  2. you can only subscribe to a SINGLE CONTEXT using context type
  
*/
export default UserContext;