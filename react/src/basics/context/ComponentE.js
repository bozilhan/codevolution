import React, {Component} from 'react';
import ComponentF from './ComponentF';
import UserContext from './userContext';

class ComponentE extends Component {
  static contextType = UserContext;

  render() {
    /*
      ComponentE is now able to render the username
    */
    return (
      <>
        <h1>Component E context value {this.context}</h1>
        <ComponentF />
      </>
    );
  }
}
/*
  context type property  there is another way to consume context value
  2. assign this user context to the context type property on the class
  outside class 
  in the render method user context value is available as this.context

  ALTERNATIVE
  sinif icerisindeki static memberi sil sinif disinda bunu ac
  ComponentE.contextType = UserContext;
*/

export default ComponentE;
