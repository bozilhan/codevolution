import React, {Component} from 'react';
import {UserConsumer} from './userContext';

class ComponentF extends Component {
  render() {
    /*
      we are passing a function AS a CHILD to the consumer component
      the function receives the context value as its parameter which can than be used
      to return the desired JSX
      you can chose to just display it or even use it for some rendering logic

      consumer component tells ComponentF hey you need the user name right
      let me provide it to you what you want to do with user name is UP TO YOU
      JUST MAKE SURE you RETURN PROPER JSX

      ComponentF will consume the username and simply render it AS PART OF the JSX
      */
    return (
      <UserConsumer>{(username) => <h1>Hello {username}</h1>}</UserConsumer>
    );
  }
}

export default ComponentF;
