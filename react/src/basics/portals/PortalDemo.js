import React from 'react';
import ReactDOM from 'react-dom';

/*
WHY do we need them
one of the use cases which is having to deal with parent component's CSS
when that child component is a modal a pop-up or tooltip
*/
const PortalDemo = () => {
  return ReactDOM.createPortal(
    <>
      <h1>Portals Demo</h1>
    </>,
    document.getElementById('portal-root')
  );
};

export default PortalDemo;
