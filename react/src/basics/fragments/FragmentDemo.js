import React from 'react';

/*
It’s a tiny bit faster and has less memory usage (no need to create an extra DOM node). This only has a real benefit on very large and/or deep trees, but application performance often suffers from death by a thousand cuts. This is one cut less.
Some CSS mechanisms like Flexbox and CSS Grid have a special parent-child relationship, and adding divs in the middle makes it hard to keep the desired layout while extracting logical components.
The DOM inspector is less cluttered.

https://dev.to/tumee/react-fragments-what-why-how-2kh1
https://getstream.io/blog/react-fragments/
https://www.jackfranklin.co.uk/blog/react-fragments/
https://frontarm.com/james-k-nelson/react-fragments-in-practice/
https://www.youtube.com/watch?v=bHdh1T0-US4&list=PLC3y8-rFHvwgg3vaYJgHGnModB54rxOk3&index=25
  fragments basically lets you GROUP a LIST OF CHILDREN ELEMENTS WITHOUT ADDING EXTRA NODES TO the DOM

  ./src/basics/fragments/FragmentDemo.js
  Line 8:33:  Parsing error: Adjacent JSX elements must be wrapped in an enclosing tag.
  Did you want a JSX fragment <>...</>?

  anytime your component has to return multiple elements
  you HAVE TO ENCLOSE(kusatmak,cevrelemek) THEM in a SINGLE PARENT ELEMENT

  we can REPLACE ENCLOSING div tag with REACT FRAGMENT and
  that will PREVENT the EXTRA NODE from being added to the dom

  If you don’t need to pass any attributes / keys,
  you can also use empty tags <></> to indicate a fragment
  there is one LIMITATION. you CAN NOT PASS in the KEY ATTRIBUTE to <></>

  KEY attribute is the ONLY attribute that can be PASSED TO a react fragment
        {items.map((item) => (
        <React.Fragment key={item.id}>
          <h1>Title</h1>
          <p>{item.title}</p>
        </React.Fragment>


<div></div>
<span></span>
KULLANMA!!!

align center vs gerekiyorsa div yerine Material UI GRID KULLAN
*/
const FragmentDemo = () => {
  return (
    <>
      <h1>Fragment Demo</h1>
      <p>This describes Fragment Demo Component</p>
    </>
  );
};

export default FragmentDemo;
