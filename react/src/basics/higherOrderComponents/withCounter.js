import React from 'react';
/*
HOC is a pattern where a function takes a COMPONENT AS ARGUMENT and RETURNS NEW COMPONENT
const NewComponent = higherOrderComponent(OriginalComponent)

HOC adds ADDITIONAL DATA OR FUNCTIONALITY to the ORIGINAL COMPONENT
So the new component can also be referred to AS ENHANCED COMPONENT

const IronMan = withSuit(TonyStark)
here TonyStark is the ORIGINAL component withSuit is the function that will enhance TonyStark and return IronMan which is the ENHANCED component

HOC accepts the ORIGINAL component AS ITS PARAMETER

!!!COK ONEMLI!!!!
when you create HOC please make sure to PASS DOWN the REST OF THE PROPS
*/
const withCounter = (WrappedComponent, incrementNumber) => {
  /*
    create NEW CLASS COMPONENT
    in the HOC
    we need to pass down the state and
    the increment method AS PROPS
    so that the ORIGINAL COMPONENT can make use of that functionality
    */
  class WithCounter extends React.Component {
    constructor(props) {
      super(props);

      this.state = {
        count: 0
      };
    }

    /*
    ALTERNATIVE
      incrementCount2 = () => {
        this.setState((prevState) => {
            return {count: prevState.count + 1};
        });
      };
    */
    incrementCount = () => {
      this.setState((prevState) => ({
        count: prevState.count + incrementNumber
      }));
    };

    render() {
      /*
        HOC INJECTS a count and incrementCount props to ANY COMPONENT required

        https://www.youtube.com/watch?v=l8V59zIdBXU&list=PLC3y8-rFHvwgg3vaYJgHGnModB54rxOk3&index=35
        when we specify props on ClickCounter component
        the props are SENT DOWN TO THE HOC!!!
        NOT TO ClickCounter

        prop is passed to the HOC but NOT TO THE COMPONENT that is WRAPPED
        to FIX this issue we need to pass down the remaining props to the wrapped component
        using the SPREAD OPERATOR

        WHAT IS HAPPENING HERE is that
        the HOC adds 2 props to the WrappedComponent and then
        simply passes down WHATEVER REMAINING PROPS have been specified

        !!!COK ONEMLI!!!!
        when you create HOC please make sure to PASS DOWN the REST OF THE PROPS
     */
      return (
        <WrappedComponent
          count={this.state.count}
          incrementCount={this.incrementCount}
          {...this.props}
        />
      );
    }
  }

  /*
     returns the new COMPONENT in the arrow function
    */
  return WithCounter;
};

export default withCounter;
