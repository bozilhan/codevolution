import React, {Component} from 'react';
import withCounter from './withCounter';

class ClickCounter extends Component {
  render() {
    const {count, incrementCount} = this.props;
    return (
      <>
        <button onClick={incrementCount}>
          {this.props.name} Clicked {count} times
        </button>
      </>
    );
  }
}

/*
instead of exporting ClickCounter we export HOC
ClickCounter now has new prop called name
so in ClickCounter we can actually render this prop
*/
export default withCounter(ClickCounter, 5);
