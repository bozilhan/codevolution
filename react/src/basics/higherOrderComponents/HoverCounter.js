import React, {Component} from 'react';
import withCounter from './withCounter';

class HoverCounter extends Component {
  render() {
    const {count, incrementCount} = this.props;
    return (
      <>
        <h2 onMouseOver={incrementCount}>Hovered {count} Times</h2>
      </>
    );
  }
}

/*
instead of exporting HoverCounter we export HOC
HoverCounter now has new prop called name
so in HoverCounter we can actually render this prop
*/
export default withCounter(HoverCounter, 10);
