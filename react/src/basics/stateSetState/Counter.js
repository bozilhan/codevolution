import React, {Component} from 'react';

class Counter extends Component {
  constructor(props) {
    super(props);

    this.state = {
      count: 0
    };
  }

  increment() {
    /*
      when we click the value is not incremented in the UI
      in the console you can see that it has changed 

      what does means is that the UI is NOT RE-RENDERING whenever the state is changing
      this is the MAIN REASON we should NEVER MODIFY THE STATE DIRECTLY
      
      this.state.count = this.state.count + 1;

      the ONLY PLACE IS CONSTRUCTOR

      any other time to change state setState METHOD HAS TO BE USED!!!

      NEVER MODIFY THE STATE DIRECTLY instead make use of setState
      when you modify the state DIRECTLY react will not RE-RENDER the component

      !!! when we click on increment the value is 1
      !!! but if you take a look at the console the value is 0
      !!! so the console value is 1 LESS THAN the RENDERED VALUE and 
      !!! this is because calls to setState are ASYNCHRONOUS
      !!! console.log is being called BEFORE the STATE IS ACTUALLY SET
      !!! you can pass in a CALLBACK function as the second parameter to the setState method

      setState method has two parameters
      1. state object
      2. the callback function. callback will be an arrow function

      ?whenever you need to execute some code after the state has been changed
      ?DO NOT PLACE that code RIGHT AFTER(hemen sonra) the setState method
      ?instead place that code WITHIN CALLBACK function
    */
    // this.setState(
    //   {
    //     count: this.state.count + 1,
    //   },
    //   () => {
    //     console.log('Callback value', this.state.count);
    //   }
    // );

    /*
      !!! we NEVER use CURRENT STATE
      !!! INSTEAD we ALWAYS USE PREVIOUS STATE
    */
    this.setState(
      (prevState, props) => ({
        count: prevState.count + 1
      }),
      () => {
        console.log('Callback value', this.state.count);
      }
    );

    console.log(
      'Outside Set state 1 LESS THAN the RENDERED VALUE ',
      this.state.count
    );
  }

  /*
    log 5 kere 0
    callback 5 kere 1
    {this.state.count} 1

    !!! because react may GROUP MULTIPLE setState calls into SINGLE UPDATE for BETTER PERFORMANCE

    ? whenever you have to update the state BASED ON THE PREVIOUS STATE
    ? we need to PASS a FUNCTION AS ARGUMENT to setState method
    ? instead of passing in an object
  */
  incrementFive() {
    this.increment();
    this.increment();
    this.increment();
    this.increment();
    this.increment();
  }

  render() {
    return (
      <div>
        <div>count - {this.state.count}</div>
        <button onClick={() => this.incrementFive()}>Increment</button>
      </div>
    );
  }
}

export default Counter;
