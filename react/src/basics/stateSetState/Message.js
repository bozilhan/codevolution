import React, {Component} from 'react';

class Message extends Component {
  constructor() {
    /* 
      this is REQUIRED because we extend react's component class 
      and a call has to be made to the base class constructor
    */
    super();

    /*
      we create our state object
      state is reserved keyword in react
      when you declare this.state
      react understands your intention
    */
    this.state = {
      /*
          initialize a property
        */
      message: 'Welcome visitor'
    };
  }

  changeMessage() {
    /*
        to change the state of the component we need to call
        setState method 
        setState accepts an object which is NEW STATE OF THE COMPONENT
      */
    this.setState({
      message: 'Thank you for subscribing'
    });
  }

  render() {
    return (
      <div>
        <h1> {this.state.message}</h1>
        <button onClick={() => this.changeMessage()}>Subscribe</button>
      </div>
    );
  }
}

export default Message;
