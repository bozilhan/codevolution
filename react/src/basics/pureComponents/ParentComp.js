import React, {Component} from 'react';
import MemoComponent from '../memo/MemoComponent';

/*
  there is a second way to create a class component
  that is by EXTENDING the PURE COMPONENT CLASS from react

  what is the difference between Component class and the pureComponent class
  and also WHEN should we use one over the other

what we are really concerned about in this example though is
WHEN the render() method is called in EACH of the component

regular component does not implement the shouldComponentUpdate method. It always returns true by default

pure component implements the shouldComponentUpdate with a SHALLOW prop and state comparison. pureComponent does a shallow of previousState with currentState and previousProps with currentProps
and only when the shallow comparison indicates THERE IS A DIFFERENCE the component WILL RE-RENDER

SHALLOW COMPARISON (SC)
PRIMITIVES(numbers, strings, boolean)
SC returns true if a and b have the SAME VALUE and are of the SAME TYPE

FOR COMPLEX TYPES(objects, arrays)
a shallow comparison b returns true if a and b REFERENCE the EXACT SAME OBJECT


WHY to use PURE COMP
pure components by PREVENTING UNNECESSARY RENDERS can give you PERFORMANCE BOOST in certain scenarios
for example you are rendering list of 50 items by not re-rendering them when it is not required they are going to have a good performance boost

It is a GOOD IDEA to ensure that ALL CHILD components are ALSO PURE TO AVOID UNEXPECTED BEHAVIOUR

Never MUTATE the STATE. ALWAYS RETURN A NEW OBJECT THAT REFLECTS THE NEW STATE

performans sikintisi oldugu dusunulen componentler pure yapilir. Gerisi icin regular class component
*/

class ParentComp extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: 'Vishvas'
    };
  }

  componentDidMount() {
    setInterval(() => {
      this.setState({
        name: 'Vishwas'
      });
    }, 2000);
  }

  render() {
    console.log(
      '************************** Parent Comp render **************************'
    );

    return (
      <>
        <h1>ParentComp</h1>
        <MemoComponent name={this.state.name} />
        {/* <RegularComp name={this.state.name} /> */}
        {/* <PureComp name={this.state.name} /> */}
      </>
    );
  }
}

export default ParentComp;
