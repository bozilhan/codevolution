/*
to style react components
1. regular css stylesheet
2. inline styling
3. css modules
4. css in JS Libraries Recommendation: Styled Components

    1. regular css stylesheet
    you can also CONDITIONALLY APPLY a class based on props or state of the component
    if you want to specify MULTIPLE CLASSES the SIMPLEST OPTION is to use TEMPLATE LITERALS
    as an ALTERNATIVE to TEMPLATE LITERALS there is also library called CLASS NAMES
*/
import React from 'react';
import './myStyles.css';

const Stylesheet = (props) => {
  let className = props.primary ? 'primary' : '';
  return (
    <div>
      <h1 className={`${className} font-xl`}>Stylesheets</h1>
    </div>
  );
};

export default Stylesheet;
