import React from 'react';

/*
to style react components
1. regular css stylesheet
2. inline styling
3. css modules
4. css in JS Libraries Recommendation: Styled Components

2. inline styling
in react inline styles ARE NOT specified AS A STRING
instead they are specified with an OBJECT
whose KEY is the CAMELCASE version of the style name and the value is usually a STRING

to apply the style INLINE we use STYLE ATTRIBUTE

3. css modules
CSS modules feature is available with REACT-SCRIPTS version 2 or higher
*/
const heading = {
  fontSize: '72px',
  color: 'blue'
};

const Inline = () => {
  return (
    <div>
      <h1 className='error'>Error From Inline</h1>
      {/* <h1 className={styles.success}>Success From Inline</h1> */}
      <h1 style={heading}>Inline</h1>
    </div>
  );
};

export default Inline;
