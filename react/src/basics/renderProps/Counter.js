/*
https://www.youtube.com/watch?v=EZil2OTyB4w&list=PLC3y8-rFHvwgg3vaYJgHGnModB54rxOk3&index=37

this Counter component will be our CONTAINER COMPONENT
where we IMPLEMENT the COMMON FUNCTIONALITY

render is a prop WHOSE VALUE is FUNCTION
the function receives the count and incrementCount method and
will RETURN ClickCounterTwo component passing the same as props
      <Counter
        render={(count, incrementCount) => (
          <ClickCounterTwo count={count} incrementCount={incrementCount} />
        )}
      />

      <Counter
        render={(count, incrementCount) => (
          <HoverCounterTwo count={count} incrementCount={incrementCount} />
        )}
      />

Even though ClickCounterTwo and HoverCounterTwo SHARE COMMON CODE,
the Counter component INSTANCE will be DIFFERENT and hence there is NO CONFLICT BETWEEN the count state values

the prop NEED NOT BE CALLED AS 'render'. it could be called ANYTHING you wish to BUT 'render' is kind of the CONVENTION

there is a variation of the render props pattern which doesnt even make use of the prop.
instead the CHILDREN prop is USED
we have to make two simple changes
instead of render prop we pass in the function in between the component OPENING and CLOSING TAGS
*/
import React, {Component} from 'react';

class Counter extends Component {
  constructor(props) {
    super(props);

    this.state = {
      count: 0
    };
  }

  /*
    ALTERNATIVE
      incrementCount2 = () => {
        this.setState((prevState) => {
            return {count: prevState.count + 1};
        });
      };
    */
  incrementCount = () => {
    this.setState((prevState) => ({
      count: prevState.count + 1
    }));
  };
  render() {
    /*
    JSX is EMPTY. because the RENDER PROP will CONTROL WHAT we will be RENDERED WITH this Counter Component

    for our example we will render ClickCounterTwo and HoverCounterTwo
    and PASS DOWN {count} state and incrementCount() method

    the Counter component is basically TELLING hey TAKE the count state and incrementCount method
    and RENDER WHATEVER YOU WANT TO

    I will handle everything regarding(iliskin,hakkinda,konusunda,dair) counter logic
    Counter component JUST WORRY ABOUT WHAT TO RENDER
    */
    return <>{this.props.render(this.state.count, this.incrementCount)}</>;
  }
}

export default Counter;
