import React, {Component} from 'react';

/*
RENDER PROPS
refers to technique for SHARING CODE between React components using PROP WHOSE VALUE IS A FUNCTION
1. SHARING CODE
2. PROP WHOSE VALUE IS A FUNCTION
another pattern for sharing code between react components

pass prop to function which will return string
<User name={(isLoggedIn) => (isLoggedIn ? 'Vishwas' : 'Guest')} />
prop olarak metod gectigimizde metodun kendisini cagirmamiz gerek
{this.props.render} --> reference to a function
{this.props.render(true)} --> call the function

in react it is possible to use prop whose VALUE IS A FUNCTION
TO CONTROL WHAT IS ACTUALLY RENDERED BY A COMPONENT
*/
class User extends Component {
  render() {
    return <h1>{this.props.render(false)}</h1>;
  }
}

export default User;
