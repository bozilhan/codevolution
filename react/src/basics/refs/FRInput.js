import React from 'react';

/*
forwardRef method takes in a component as its parameter
when a component is passed as a parameter to the createRefMethod
it receives the ref attribute as its SECOND parameter

we can use this ref parameter and pass is as a value to the ref attribute
on the native input element

this ref parameter will point to the value of the ref attribute
from the parent component
ref is being forwarded from the PARENT component to the native input element
*/
const FRInput = React.forwardRef((props, ref) => {
  return (
    <>
      <input type='text' ref={ref} />
    </>
  );
});

export default FRInput;
