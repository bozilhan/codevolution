/*
Forward Ref Input
ref forwarding is a technique for AUTOMATICALLY passing a ref
through a component to one of its children
*/
import React, {Component} from 'react';
import FRInput from './FRInput';

class FRParentInput extends Component {
  constructor(props) {
    super(props);
    this.inputRef = React.createRef();
  }

  handleClick = () => {
    /*
    because we use forwardRef technique
    ref.current points to the native input element
    NOT the FRInput component instance
      */
    this.inputRef.current.focus();
  };

  render() {
    return (
      <>
        <FRInput ref={this.inputRef} />
        <button onClick={this.handleClick}>Focus Input</button>
      </>
    );
  }
}

export default FRParentInput;

/*
react portals provide a way to render children into a dom node that exists outside the dom hierarchy of the parent component

portals provide the ability to break out of the DOM tree
so you can render a component onto a DOM node that is not under this root element
*/
