import React, {Component} from 'react';
import Input from './Input';

class FocusInput extends Component {
  constructor(props) {
    super(props);

    this.componentRef = React.createRef();
  }
  clickHandler = () => {
    /*
      focusInput() is defined in Input.js
      */
    this.componentRef.current.focusInput();
  };

  render() {
    return (
      <>
        <Input ref={this.componentRef} />
        <button onClick={this.clickHandler}>Focus Input</button>
      </>
    );
  }
}

export default FocusInput;
