/*
refs make it POSSIBLE TO ACCESS DOM nodes DIRECTLY within REACT
*/
import React, {Component} from 'react';

class RefsDemo extends Component {
  constructor(props) {
    super(props);

    /*
      1. create a ref
      it is common to create refs in the constructor
      so that they can be referenced through the component
      */
    this.inputRef = React.createRef();
  }

  componentDidMount() {
    /*
    3. call the focus() method on this input element
    */
    this.inputRef.current.focus();
    /*
      you can see that we have an OBJECT
      if you expand it you can see a property called CURRENT of TYPE input
      this CURRENT property POINTS TO ACTUAL DOM NODE
      */
    console.log('this.inputRef', this.inputRef);
  }
  clickHandler = () => {
    alert(this.inputRef.current.value);
  };

  render() {
    /*
    2. attach this ref to input element in the render method
    ref is RESERVED keyword
    */
    return (
      <>
        <input type='text' ref={this.inputRef} />
        <button onClick={this.clickHandler}>Click</button>
      </>
    );
  }
}

export default RefsDemo;
