/*
https://dmitripavlutin.com/use-react-memo-wisely/
React.memo
what pure components is to class based components
memo is to functional components
*/
import React from 'react';

const MemoComponent = ({name}) => {
  console.log('RENDERING MEMO COMPONENT');

  return (
    <>
      <h1>{name}</h1>
    </>
  );
};

export default React.memo(MemoComponent);
