/*
  1. if/else approach

  what i want is the message to be CONDITIONALLY RENDERED
  based on the isLoggedIn state

  if i am logged in the message welcome wishwas should be displayed
  if i am not logged in the message welcome guest should be displayed

  if/else statements DONT WORK INSIDE the JSX

  2. using element variables approach
  in this approach you use javascript variables to store elements

  3. ternary operator approach
  it can be used INSIDE JSX. you might follow most of time KEEPS CODE SIMPLE AND READABLE

  4. short circuit operator approach
  this approach is just a specific case of the ternary operator approach

  3. ve 4. yontem tercih edilir
*/
import React, {Component} from 'react';

class UserGreeting extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoggedIn: false
    };
  }

  render() {
    /*
    1. if/else approach
      if (this.state.isLoggedIn) {
          return <div>Welcome Wishwas</div>;
      } else {
          return <div>Welcome Guest</div>;
      }

      2. using element variables approach
      message is the variable which stores the element TO BE RENDERED
      let message;
      if (this.state.isLoggedIn) {
        message = <div>Welcome Wishwas</div>;
      } else {
        message = <div>Welcome Guest</div>;
      }
      return <div>{message}</div>;

      3. within the parenthesis we use the CONDITIONAL OPERATOR
      return this.state.isLoggedIn ? (
          <div>Welcome Wishwas</div>
        ) : (
          <div>Welcome Guest</div>
        );

      4. the expression first evaluates the LHS of the operator
      if this.state.isLoggedIn is true it evaluates the RHS
      which in our case JSX thay will be rendered in the browser
      However if LHS is FALSE RHS is never evaluated

    */

    return this.state.isLoggedIn && <div>Welcome Wishwas</div>;
  }
}

export default UserGreeting;
