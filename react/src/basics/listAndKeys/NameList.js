import React from 'react';
import Person from './Person';

/*
  LIST RENDERING
  go over EACH ELEMENT IN THE ARRAY and APPLY TRANSFORMATION SPECIFIED IN THE ARROW FUNCTION BODY

  RECOMMENDED way is to refactor the jsx INTO SEPARATED COMPONENT
  const nameList = names.map((name) => <h2>{name}</h2>);

*/
const NameList = () => {
  const names = ['Bruce', 'Clark', 'Diana', 'Bruce'];

  const persons = [
    {
      id: 1,
      name: 'Bruce',
      age: 37,
      skill: 'React'
    },
    {
      id: 2,
      name: 'Clark',
      age: 25,
      skill: 'Angular'
    },
    {
      id: 3,
      name: 'Diana',
      age: 28,
      skill: 'Vue'
    }
  ];

  /*
  index.js:1 Warning: Each child in a list should have a unique "key" prop.
  each item in the list rendered using map operator should HAVE A PROP CALLED KEY
  and the VALUE to that prop should BE UNIQUE within the list
  key={person.id}
  prop ismi KEY OLMALI!!!
  key is a SPECIAL string attribute you NEED TO INCLUDE when CREATING LIST OF ELEMENTS
  keys give the elements a stable identity
  keys help react identify which items have changed, are added, or are removed
  this results in a much more efficient update of the UI

  key prop value dont need to BE ID, It could even be the NAME
  it can be anything AS LONG AS YOU KNOW FOR SURE IT IS UNIQUE within the list!!!

  key prop is a special attribute you NEED TO INCLUDE WHEN CREATING LISTS OF ELEMENTS

  key props ARE NOT ACCESSIBLE in the CHILD COMPONENTS

  WHY do we need key prop
  keys help react identify which items in a list have changed or added or removed and
  place a crucial role IN HANDLING UI UPDATES EFFICIENTLY
  https://www.youtube.com/watch?v=0sasRxl35_8&list=PLC3y8-rFHvwgg3vaYJgHGnModB54rxOk3&index=18 04:15/07:31 detayli aciklama
  */

  /*
    you can use index as a key provided
    your list satisfies certain conditions
    1. items in your list DO NOT HAVE a UNIQUE ID. if the items have unique id ALWAYS go with that
    2. the list is STATIC LIST and will NOT CHANGE. For example you never add items to the list or remove items from the list
    3. the list NEVER BE REORDERED or FILTERED for example sorting based on a column

    When your list SATISFIES ALL the 3 CONDITIONS, can safely use the index as a key prop

    if your list NEVER SATISFIES ALL the 3 CONDITIONS, you try HASHING out unique value form ONE OF THE EXISTING props

    value or filtering based on user inputs
    dizide ayni elemandan birden fazla varsa
    key prop olarak namei eklemek hata verir
    dolayisiyla UNIQUE ID OLMAYAN data setler icin map metodda INDEX PARAMETRESI kullanilir

    using index as a key will CAUSE SOME SERIOUS UI ISSUES in certain(belirli) scenarios

    https://www.youtube.com/watch?v=xlPxnc5uUPQ&list=PLC3y8-rFHvwgg3vaYJgHGnModB54rxOk3&index=19 04:50/10:59 aciklama
  */
  const nameList = names.map((name, index) => (
    <h2 key={index}>
      index: {index} name: {name}
    </h2>
  ));

  const personList = persons.map((person) => (
    <Person key={person.id} person={person} />
  ));

  return (
    <div>
      <div>{nameList}</div>
      <div>{personList}</div>
    </div>
  );
};

export default NameList;
