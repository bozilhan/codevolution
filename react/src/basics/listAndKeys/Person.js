import React from 'react';

/*
key props ARE NOT ACCESSIBLE in the CHILD COMPONENTS
{key} is NOT RENDERED
      <h2>
        {key} I am {person.name}. I am {person.age} years old. I know{' '}
        {person.skill}
      </h2>
UI
I am Bruce. I am 37 years old. I know React
I am Clark. I am 25 years old. I know Angular
I am Diana. I am 28 years old. I know Vue

index.js:1 Warning: Person: `KEY` IS NOT A PROP. Trying to access it will result in `undefined` being returned.
If you need to access the same value within the child component, you should pass it as a different prop.

React tells us key prop is SOMETHING I NEED TO RENDER THE LIST EFFICIENTLY
if you trying to pass down a value to be used in the child component
pass it AS A DIFFERENT PROP. key prop is RESERVED. so in any of your components DO NOT TRY TO USE key prop to render any data

*/
const Person = ({person}) => {
  return (
    <div>
      <h2>
        I am {person.name}. I am {person.age} years old. I know
        {person.skill}
      </h2>
    </div>
  );
};

export default Person;
