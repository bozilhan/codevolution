import React, {Component} from 'react';

/*
https://www.youtube.com/watch?v=7Vo_VCcWupQ&list=PLC3y8-rFHvwgg3vaYJgHGnModB54rxOk3&index=21
CONTROLLED COMPONENTS
the form elements whose value is controlled by react is called a CONTROLLED COMPONENT

consider input element.
the input element have a value(value prop).
the input elements value can also change based on user interaction(onChange prop).

in a controlled component the the value of the input field is SET TO THE STATE PROPERTY
next we have onChange event fired
whenever there is a change in the input field's value in the onChange handler
we use SETSTATE() method TO UPDATE THE STATE.
when the state gets updated the render method is called and the new state is assigned as a value to the input element

to convert regular HTML tag into CONTROLLED REACT COMPONENT
1. create a component state that we control the value of the input element
2. whenever there is a change in the input field's value in the onChange handler, we use SETSTATE() method TO UPDATE THE STATE.


when clicking submit button the page refreshes <button type='submit'>Submit</button>
*/
class Form extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      comments: '',
      topic: 'react'
    };
  }

  handleUsernameChange = (event) => {
    this.setState({
      username: event.target.value
    });
  };

  handleCommentsChange = (event) => {
    this.setState({
      comments: event.target.value
    });
  };

  handleTopicChange = (event) => {
    this.setState({
      topic: event.target.value
    });
  };

  handleSubmit = (event) => {
    alert(`${this.state.username} ${this.state.comments} ${this.state.topic}`);
    event.preventDefault();
  };

  render() {
    const {username, comments, topic} = this.state;
    return (
      <form onSubmit={this.handleSubmit}>
        <div>
          <label>Username</label>
          <input
            type='text'
            value={username}
            onChange={this.handleUsernameChange}
          />
        </div>
        <div>
          <label>Comments</label>
          <textarea
            value={comments}
            onChange={this.handleCommentsChange}
          ></textarea>
        </div>
        <div>
          <label>Topic</label>
          <select value={topic} onChange={this.handleTopicChange}>
            <option value='react'>React</option>
            <option value='angular'>Angular</option>
            <option value='vue'>Vue</option>
          </select>
        </div>
        <button type='submit'>Submit</button>
      </form>
    );
  }
}

export default Form;
