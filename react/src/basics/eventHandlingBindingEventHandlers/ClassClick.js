import React, {Component} from 'react';

class ClassClick extends Component {
  /*
    WITH PARENTHESIS it becomes FUNCTION CALL and that is not WHAT WE WANT
    onClick={this.clickHandler()}

    when your clickHandler CHANGES the STATE of the COMPONENT
    the component CONSTANTLY RE-RENDERS and you might see an INFINITE NUMBER OF MESSAGES

    we want the handler to be a function NOT A FUNCTION CALL
    !!!DO NOT ADD THE PARENTHESIS!!!

    generally event handlers tend to MODIFY the state of the component using
    this.setState() method

    when try to do onClick={this.clickHandler()}
    you run into(karsilasmak) a whole world of CONFUSION
    all that confusion revolves around THIS KEYWORD BINDING in JAVASCRIPT RELATED
    NOT REACT SPECIFIC!!!
    */
  clickHandler() {
    console.log('Clicked the button');
  }
  render() {
    return (
      <div>
        <button onClick={this.clickHandler}>Click Me</button>
      </div>
    );
  }
}

export default ClassClick;
