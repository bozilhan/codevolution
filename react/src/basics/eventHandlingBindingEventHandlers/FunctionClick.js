import React from 'react';

/*
  when the user clicks on this button
  a click event is fired 

  our goal is to capture that click event and 
  execute some basic code

  WITH PARENTHESIS it becomes FUNCTION CALL and that is not WHAT WE WANT
  onClick={clickHandler()}
  bu sekildeyken acilista bir SADECE 1 KEZ CAGRILIR. click yaptigimizda da CAGRILMAZ

  we want the handler to be a function NOT A FUNCTION CALL
  !!!DO NOT ADD THE PARENTHESIS!!!
*/
const FunctionClick = () => {
  const clickHandler = () => {
    console.log('Button Clicked');
  };

  return (
    <div>
      <button onClick={clickHandler}>Click</button>
    </div>
  );
};

export default FunctionClick;
