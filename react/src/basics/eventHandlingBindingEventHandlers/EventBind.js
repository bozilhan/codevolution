import React, {Component} from 'react';

class EventBind extends Component {
  constructor(props) {
    super(props);

    this.state = {
      message: 'Hello'
    };
  }

  /*
  clickHandler() {
    onClick={this.clickHandler}

    this keyword within our event handler is UNDEFINED
     JAVASCRIPT RELATED NOT REACT SPECIFIC!!!
     read this keyword for in javascript

    this.setState({
      message: 'Goodbye'
    });

    console.log(this); // undefined

    therefore EVENT BINDING IS NECESSARY in CLASS COMPONENTS

    1. use bind keyword and bind the handler in the render method
        onClick={this.clickHandler.bind(this)}
            EVERY UPDATE to the STATE will CAUSE the component to RE-RENDER
            this in turn generate a BRAND NEW event handler on every render
            performance penalty and problem for child components
            NOT USE because of PERFORMANCE IMPLICATIONS(etki)


    2. use arrow functions in the render method
        onClick={() => this.clickHandler()}
            we dont need curly braces or the return keyword for thw arrow function body
            that is because the function BODY a SINGLE STATEMENT
            we call the event handler and returning this.clickHandler()
            that is wh PARENTHESIS is REQUIRED in this approach
            EASIEST WAY TO PASS PARAMETERS USE this for passing parameters
            if your component DOES NOT INVOLVE RENDERING NESTED CHILDREN components
            this approach is still viable(gecerli)

    3. binding event handler in the CONSTRUCTOR as opposed to binding in the render method
        this.clickHandler = this.clickHandler.bind(this);
        <button onClick={this.clickHandler}>Click</button>
            binding in the class constructor
            Official React Documentation
            because the binding HAPPENS ONCE in the constructor,
            this is BETTER compared to binding in the render method
            BEST OPTION

    4. use arrow function as a class property
    basically change the way you define your method in the class
        clickHandler = () => {
            this.setState({
                message: 'Goodbye'
            });
        };
        <button onClick={this.clickHandler}>Click</button>
            BEST OPTION

    this.setState({
        message: 'Goodbye'
    });

    console.log(this);
}
  */

  clickHandler = () => {
    this.setState({
      message: 'Goodbye'
    });
  };

  render() {
    return (
      <div>
        <div>{this.state.message}</div>
        <button onClick={this.clickHandler}>Click</button>
      </div>
    );
  }
}

export default EventBind;
