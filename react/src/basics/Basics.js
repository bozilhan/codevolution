import React from 'react';
// import {Greet} from './props/GreetFunctional';
// import GreetClass from './props/GreetClass';
// import Message from './stateSetState/Message';
// import Counter from './stateSetState/Counter';
// import FunctionClick from './eventHandlingBindingEventHandlers/FunctionClick';
// import ClassClick from './eventHandlingBindingEventHandlers/ClassClick';
// import EventBind from './eventHandlingBindingEventHandlers/EventBind';
// import ParentComponent from './methodsAsProps/ParentComponent';
// import UserGreeting from './rendering/UserGreeting';
// import NameList from './listAndKeys/NameList';
// import Stylesheet from './styling/Stylesheet';
// import Inline from './styling/Inline';
// import './styling/basicStyles.css';
// import styles from './styling/basicStyles.module.css';
// import Form from './forms/Form';
// import FragmentDemo from './fragments/FragmentDemo';
// import Table from './fragments/Table';
// import PureComp from './pureComponents/PureComp';
// import ParentComp from './pureComponents/ParentComp';
// import RefsDemo from './refs/RefsDemo';
// import FocusInput from './refs/FocusInput';
// import FRParentInput from './refs/FRParentInput';
// import PortalDemo from './portals/PortalDemo';
// import ClickCounter from './higherOrderComponents/ClickCounter';
// import HoverCounter from './higherOrderComponents/HoverCounter';
// import User from './renderProps/User';
// import HoverCounterTwo from './renderProps/HoverCounterTwo';
// import ClickCounterTwo from './renderProps/ClickCounterTwo';
// import Counter from './renderProps/Counter';
// import CounterWithChildren from './renderProps/CounterWithChildren';
import ComponentC from './context/ComponentC';
import {UserProvider} from './context/userContext';

/*
    <Greet name='Bruce' />
we are sending some information or some property to Greet component
*/
const Basics = () => {
  /*
  wrap <ComponentC /> with UserProvider react component
  */
  return (
    <>
      <UserProvider value='Vishwas'>
        <ComponentC />
      </UserProvider>
      {/* <Counter
        render={(count, incrementCount) => (
          <ClickCounterTwo count={count} incrementCount={incrementCount} />
        )}
      />

      <Counter
        render={(count, incrementCount) => (
          <HoverCounterTwo count={count} incrementCount={incrementCount} />
        )}
      />

      <CounterWithChildren>
        {(count, incrementCount) => (
          <ClickCounterTwo count={count} incrementCount={incrementCount} />
        )}
      </CounterWithChildren>

      <CounterWithChildren>
        {(count, incrementCount) => (
          <HoverCounterTwo count={count} incrementCount={incrementCount} />
        )}
      </CounterWithChildren> */}

      {/* <ClickCounterTwo />
      <HoverCounterTwo />
      <User render={(isLoggedIn) => (isLoggedIn ? 'Vishwas' : 'Guest')} /> */}
      {/* <ClickCounter name='Vishwas' /> */}
      {/* <HoverCounter /> */}
      {/* <PortalDemo /> */}
      {/* <FRParentInput /> */}
      {/* <FocusInput /> */}
      {/* <RefsDemo /> */}
      {/* <ParentComp /> */}
      {/* <FragmentDemo /> */}
      {/* <Table /> */}
      {/* <Form /> */}
      {/* <h1 className='error'>Error</h1> */}
      {/* <h1 className={styles.success}>Success</h1> */}
      {/* <Inline /> */}
      {/* <Stylesheet primary={true} /> */}
      {/* <NameList /> */}
      {/* <UserGreeting /> */}
      {/* <ParentComponent /> */}
      {/* <EventBind /> */}
      {/* <ClassClick />
      <FunctionClick />
      <Counter />
      <Message />
      <Greet name='Bruce' heroName='Batman'>
        <p>This is children props</p>
      </Greet>
      <Greet name='Clark' heroName='Superman'>
        <button>Action</button>
      </Greet>
      <Greet name='Diana' heroName='Wonder Woman' />

      <GreetClass name='Bruce' heroName='Batman' />
      <GreetClass name='Clark' heroName='Superman' />
      <GreetClass name='Diana' heroName='Wonder Woman' /> */}
    </>
  );
};

export default Basics;
