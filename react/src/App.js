import React from 'react';
import UseStateHookCounterArrayToString from './hooks/use-state/UseStateHookCounterArrayToString';

const App = () => {
  return (
    <div align='center'>
      <UseStateHookCounterArrayToString />
      {/* <Basics /> */}
    </div>
  );
};

export default App;
