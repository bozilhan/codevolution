Redux is a predictable state container for JS apps
redux is not ties to react. can be used with react angular vue or even vanilla javascript. Redux is library for JS applications

Redux stores and manage the state of your application
the state of application is simply the represented by all the individual components of that application this includes the data and the UI logic

in redux ALL STATE TRANSITIONS are explicit and it is possible to KEEP TRACK OF THEM. The changes to your application's state become predictable

if you manage the state of your application in a predictable way, redux can help you

Neden redux
05:35 https://www.youtube.com/watch?v=9boMnm5X9ak&list=PLC3y8-rFHvwheJHvseC3I0HuYI2f46oAK

With redux your state is contained outside your components if ComponentA wants to update the state it COMMUNICATES WITH THE STATE CONTAINER. The state container updates the state predictable(tahmin edilebilir, onceden kestirilebilir) manner(tarz/tavir/tutum/davranis) and then SENDS this value to only those components that are in need of that value

React-redux is the official redux UI binding library for react

if you are using react and redux together you should also use REACT-REDUX to bind the two libraries

THREE CORE CONCEPTS IN REDUX
https://www.youtube.com/watch?v=3rlUADfuKhQ&list=PLC3y8-rFHvwheJHvseC3I0HuYI2f46oAK&index=3
A store --> that holds the state of your application
            birden fazla componentte kullanilan herhangi bir datanin sahibidir.
            ilgili propun state degisiminin IDAMESI burada gerceklestirilir

An Action --> that describes the changes in the state of the application
              uygulamanin stateindeki DEGISIKLIKLERIN TANIMLANDIGI YER
              type + payload dan olusan PLAIN JS OBJECT statei degismesi gereken veri payloaddadir.

                ACTION CREATOR --> type ve payload propertylerinden olusan plain javascript object donen metod
                    const myActionCreator = () => {
                        return {
                            type: 'CONSTANT_STRING_LITERAL',
                            payload:
                        };
                    };

                Action Creator kullanmanin faydasi her yere ilgili nesneyi yapistirmak yerine ilgili nesneyi merkezi bir metoddan yonetmek. Ilerleyen gunlerde ilgili nesnenin tipi ve/veya payloadi degisirse sadece metodun icini degistirerek az degisiklikle durumu idare ederiz.

A reducer which actually carries out(basarmak,gerceklestirmek) the state transition depending on the action
    (stateOfTheApplicationBEFOREMakingANYChange, action) => newState


4. Three Principles
these principles basically describe the redux pattern
First Principle: the state of your whole application is stored in an object within a SINGLE STORE. Maintain our application state in a single object which would be managed by the redux store

Second Principle: the only way to change the state is to emit an action, an object describing what happened. To update the state of your app, you need to let redux know about that with an action. Not allowed to directly update the state object

Third Principle: To specify how state tree is transformed by actions, you write pure reducers. we need to write pure reducers to determine how the state changes pure reducers are basically pure functions that take THE PREVIOUS STATE and ACTION as inputs and RETURN THE NEXT STATE. reducer INSTEAD OF UPDATING THE PREVIOUS STATE should RETURN A NEW STATE

ONEMLI VISUALIZE 3 PRINCIPLES FUNDAMENTALS(TEMELLER) OF REDUX
we start off with our application which is a simple JS application the state of the application is maintained SEPARATELY in the REDUX STORE. our application is always SUBSCRIBED(abone olmak) to this redux store however the app CANNOT DIRECTLY UPDATE THE STATE. if the application WANTS TO UPDATE THE STATE it HAS TO EMIT(yaymak) or DISPATCH(gondermek) an ACTION. Once(-ir -mez, olur olmaz, hemen, bir kere) an action has been dispatched the REDUCER then handles(islemek,ele almak) that action and UPDATES the CURRENT STATE. As soon as the state is updated the value is then passed on to the application because the app is subscribed to the store

5. Actions
the ONLY WAY your application can INTERACT with the STORE
actions carry some information from your app to the redux store
actions are PLAIN JS OBJECTS
action objects have a 'type' property that indicates the type of action BEING PERFORMED
type property is TYPICALLY defined as STRING CONSTANTS

https://www.youtube.com/watch?v=8zPyXAWS0L4&list=PLC3y8-rFHvwheJHvseC3I0HuYI2f46oAK&index=11
REDUX MIDDLEWARE
Middleware is the suggested way to extend Redux with custom functionality
If you want redux with EXTRA FEATURES middleware is the way to go
Middleware provides a third-party extension point between dispatching an action and the moment it reaches the reducer
Use middleware for logging, crash reporting, performing asynchronous tasks etc.

1. import applyMiddleware
2. pass it as an argument to createStore
3. pass in the middleware to the applyMiddleware method

https://www.youtube.com/watch?v=yGyj0mSfVuk&list=PLC3y8-rFHvwheJHvseC3I0HuYI2f46oAK&index=12
Synchronous Actions as soon as an action was dispatched, the state was immediately updated

Async Actions
Asynchronous API calls to FETCH data from an END POINT and use that data in your application

the application we are going to build simply fetches a list of users from an API endpoint and stores it in the REDUX STORE
1. HOW OUR STATE IS GOING TO LOOK LIKE
2. WHAT ARE THE DIFFERENT ACTIONS
3. HOW THE REDUCER WOULD WORK

Typically for data fetching we go 3 properties for the state object
the first property is a LOADING_FLAG which indicates whether the data is currently being fetched or not. If you have an application with UI this flag would help you display a LOADING SPINNER in your component
the next property we have is the data itself
the final property is error message. Our API request might fail for some reason in that scenario instead of getting back the data we'll get back an error which we store in the error property

we have 3 actions in our application
the first action is to fetch the list of users FETCH_USERS_REQUEST
the second and third actions are dependent on this first action if the data is fetched successfully we have an action FETCH_USERS_SUCCESS
if there was some error fetching the data we have an action with type FETCH_USERS_FAILURE

reducer function
case: FETCH_USERS_REQUEST
loading:true
case: FETCH_USERS_SUCCESS
loading:false
users:data(from API)
case: FETCH_USERS_FAILURE
loading:false
error:error(from API)

redux-thunk
define async action creators
redux-thunk library basically is a middleware we will be applying to our redux store
